\section{The evaluation mechanism}
\label{sec:eval}

Evaluation functions have been introduced in~\cite{FerFioFio:2015} to get a
terminating proof-search procedure for Gentzen sequent calculus $\LJ$
for propositional intuitionistic logic. Here, we adapt this mechanism
to the case of classical propositional logic.

%%We define the evaluation
%%for $\CPL$ to get a terminating, deterministic and complete decision
%%procedure based on the Hilbert calculus $\Hc$.

Let $\Lcaltf$ be the language obtained by adding to $\Lcal$ the
constants $\true$ (true) and $\false$ (false), with the usual
interpretation; let $H$ be a formula of $\Lcaltf$ and let $\G$ be a
set of formulas of $\Lcal$.  The {\em evaluation of $H$ in $\G$},
denoted by $\eval(H,\G)$, is the formula of $\Lcaltf$ obtained by
replacing every subformula $K$ of $H$ such that $K$ is an axiom or
$K\in\G$ with $\true$ and then by performing some truth preserving
boolean simplifications (for instance, any subformula of the kind
$\true \lor K$ is replaced by $\true$, while $\false\lor K$ is
replaced by $K$).  The function $\eval$ and the auxiliary function
$\simpl$ are defined by mutual induction in Fig.~\ref{fig:eval}.  We
point out that $\eval(H,\G)$ is either $\true$ or $\false$ or a
formula of $\Lcal$ (not containing the constants $\true$, $\false$).
Moreover, $\size{\eval(H,\G)} \leq \size{H}$.

\begin{figure*}[t]
  \small
  \[
  \begin{array}{c}
    \eval(H,\G) \;=\; 
    \begin{cases}
      \true & \mbox{if $H$ is an axiom or $H\in\G$}
      \\
      \false & \mbox{if $\neg H\in\G$ }
      \\
      H & \mbox{if $H\;\in\;(\PV\setminus\G)\,\cup\,\{\true,\false\}$} 
      \\
      \simpl(H,\G) & \mbox{otherwise}
    \end{cases}
    \\[8ex]
    %% 
    \textrm{in the following $K_1\,=\,\eval(H_1,\G)$ and  $K_2\,=\,\eval(H_2,\G)$}
    \\[3ex]
    %% 
    \simpl(\neg H_1,\G)\;=\;
    \begin{cases}
      \true & \mbox{if $K_1=\false$}
      \\
      \false & \mbox{if $K_1=\true$}
      \\
      \neg K_1  & \mbox{otherwise}
    \end{cases}
                  \qquad
                  %% 
                  \simpl(H_1\land H_2,\G)\;=\;
                  \begin{cases}
                    \true & \mbox{if $K_1=\true$ and $K_2=\true$}
                    \\
                    \false & \mbox{if $K_1=\false$ or $K_2=\false$ }
                    \\
                    K_1\land K_2  & \mbox{otherwise}
                  \end{cases}
    \\[6ex]
    %% 
    \simpl(H_1\lor H_2,\G)\;=\;
    \begin{cases}
      \true & \mbox{if $K_1=\true$ or $K_2=\true$}
      \\
      \false & \mbox{if $K_1=\false$ and $K_2=\false$ }
      \\
      K_1\lor K_2  & \mbox{otherwise}
    \end{cases}
    \qquad
    %% 
    \simpl(H_1\to H_2,\G)\;=\;
    \begin{cases}
      \true & \mbox{if $K_1=\false$ or $K_2=\true$}
      \\
      \false & \mbox{if $K_1=\true$ and $K_2=\false$ }
      \\
      K_1\to K_2  & \mbox{otherwise}
    \end{cases}
    % where $K_1=\eval(H_1,\G)$ and  $K_2=\eval(H_2,\G)$
  \end{array}
  \]
  %% 
  \caption{Definition of $\eval$}
  \label{fig:eval}
\end{figure*}


One can easily prove that, assuming $\G$,
the formulas $H$, $\eval(H,\G)$ and $\simpl(H,\G)$
are classically equivalent, as stated in the next lemma

\begin{lemma}\label{lemma:eval}
  Let $\G$ be a set of formulas of $\Lcal$ and $H$ a formula
  of $\Lcaltf$. Then:
  \begin{enumerate}
  \item  $\G\sat H\Iff \eval(H,\G)$;

  \item $\G\sat H\Iff \simpl(H,\G)$.
  \end{enumerate}
  
\end{lemma}

  
By Lemma~\ref{lemma:eval} we immediately get:

\begin{theorem}\label{th:sat}
  \label{th:eval}
  Let $\G$ be a set of formulas  of $\Lcal$, let $A$ be a formula of $\Lcal$  and $\Mcal$ a model
  such that $\Mcal\sat \Gamma$.
  \begin{enumerate}
  \item  \label{th:eval:1}
    $\eval(A, \G)=\true$ implies  $\Mcal\sat A$.

 \item  \label{th:eval:2}
    $\eval(A, \G)=\false$ implies  $\Mcal\sat\neg A$.
  \end{enumerate}
\end{theorem}

By $\Lneg$ we denote the set of formulas $H$ such that
$H$ is a literal or $H=\neg(A\lor B)$.
A set of formulas $\G$ is \emph{reduced} iff the following properties hold:

\begin{itemize}
\item $\G\subseteq \Lneg$;

\item for every $p\in\PV$, $p\in\G$ implies $\neg p\not\in\Gamma$;

\item for every $H=\neg(A\lor B)\in\G$, $\eval(H,\G\setminus\{H\})=\true$.
\end{itemize}

By Theorem~\ref{th:sat}, we get:

\begin{theorem}
  \label{th:reduced}
  Let $\G$ be a reduced set of formulas. 
Then, $\G\cap\PV$ is a model of $\G$.
\end{theorem}

\begin{proof}
  Let $\Theta$ be the subset of $\G$ containing all the formulas of
  the kind $\neg(A\lor B)$ of $\G$.  We prove the theorem by induction
  on the cardinality of $\Theta$.  If $\Theta$ is empty, then $\G$
  only contains propositional variables or formulas $\neg p$ such that
  $p\in\PV\setminus \G$; this implies that $\G\cap\PV$ is a model of
  $\G$.  Let $\neg(A\lor B)\in\Theta$, let $\G_1 =
  \G\setminus\{\neg(A\lor B)\}$ and let $\Mcal$ be the model
  $\G\cap\PV$.  By induction hypothesis, $\Mcal$ is a model of $\G_1$.
  Since $\G$ is reduced, we have $\eval(\neg(A\lor B),\G_1)=\true$;
  by Theorem~\ref{th:sat} we get $\Mcal\sat \neg(A\lor B)$, hence
  $\Mcal$ is a model of $\G$.\qed
\end{proof}

A set of formulas $\G$ is \emph{contradictory} iff there exists a
formula $X$ such that $\{X,\neg X\}\subseteq \G$.  In the proof of
termination of the proof-search procedure $\Hp$, we need the following
properties of $\eval$.

\begin{lemma}\label{lemma:preserv}
  Let $\G$ be a non-contradictory set  of formulas,
  let $H$ be a formula and
let us assume that $\eval(H,\G)=\tau$, where
$\tau\in\{\true,\false\}$.
Let $K\in\G$ and  $\D=\G\setminus\{K\}$.
Then:

  \begin{enumerate}
  \item If $K=\neg\neg A$ and  $\D\cup \{A\}$  is not contradictory, then\\
    $\eval(H,\, \D\cup \{A\})=\tau$.

  \item If $K=A\land B$ and  $\D\cup \{A,B\}$ is not contradictory, then\\
     $\eval(H,\,\D\cup \{A,B\})=\tau$.

\item If $A_0\lor A_1\in\G$  and   $,\D\cup \{A_k\}$   is not contradictory, then   \\
  $\eval(H,\,\D\cup \{A_k\})=\tau$.

\item If $K=A\to B$ and  $\D\cup \{\neg A\}$    is not contradictory, then\\
  $\eval(H,\,\D\cup \{\neg A\})=\tau$.

\item If $K=A\to B$ and $,\D\cup \{B\}$
is not contradictory,  then\\
  $\eval(H,\,\D\cup \{B\})=\tau$.

\item If $K=\neg(A_0\land A_1)$  and $k\in\{0,1\}$ and
$\D\cup \{\neg A_k\}$
is not contradictory, then
  $\eval(H,\,\D\cup \{\neg A_k\})=\tau$.

\item If $K=\neg(A_0\lor A_1)$  and
  $\eval(K,\D)=\true$ and $k\in\{0,1\}$ and
$\D\cup \{\neg A_k\}$
is not contradictory, then
  $\eval(H,\,\D\cup \{\neg A_k\})=\tau$.

\item If $K=\neg(A_0\lor A_1)$  and
  $\eval(A_0,\D)=\eval(A_1,\D)=\false$, then 
  $\eval(H,\,\D)=\tau$.

  
\item  If $\neg(A\to B)\in\G$ and 
$\D\cup \{A,\neg B\}$
is not contradictory, then 
  $\eval(H,\,\D\cup \{A,\neg B\})=\tau$.


  
  \end{enumerate}

  
\end{lemma}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "hilbertProcedureEval"
%%% End: 


