:-module(basicPS, [hilbertPS/3]).
:-use_module(classicalAxioms).
:-use_module(dedThm).

/*
  Base case: the goal is an axiom.

*/

hilbertPS(InProof, Goal, Result):-
	isAxiom(Goal, _),
	!,
	newKey(Label),
	Line = line(Label, Goal, axiom),
	append(InProof, [Line], Result).
	

/*

  Base case: the goal has been already proved.

  Return the lines of InProof from the first to the one containing Goal included.

*/

hilbertPS(InProof, Goal, Result):-
	memberchk(line(Label, Goal, Type), InProof),
	!,
	headList(line(Label, Goal, Type), InProof, Result).



/*

  Base case: contradiction proved:
  
*/

hilbertPS(InProof, Goal, Result):-
	member(line(LabelB, B, TypeB), InProof),
	member(line(LabelNonB, non(B), TypeNonB), InProof),
	!,
	(
	 Goal = false,
	 !,
	 LineB = line(LabelB, B, TypeB),
	 LineNonB = line(LabelNonB, non(B), TypeNonB),

	 /* Produce a proof that concludes with a contradictory conjunction */
	 
	 deduceContradictoryConjunction(InProof, [LineB, LineNonB], Result)
	
	;

	 instanceAx7(non(Goal), B, Formula1),
	 newKey(Label1),
	 Line1 = line(Label1, Formula1, axiom),
	 
	 instanceAx1a(B, non(Goal), Formula2),
	 newKey(Label2),
	 Line2 = line(Label2, Formula2, axiom),

	 newKey(Label3),
	 applyMp(B, Formula2, Formula3),
	 Line3 = line(Label3, Formula3, mp(LabelB, Label2)),

	 newKey(Label4),
	 applyMp(Formula3, Formula1, Formula4),
	 Line4 = line(Label4, Formula4, mp(Label3, Label1)),

	 newKey(Label5),
	 instanceAx1a(non(B), non(Goal), Formula5),
	 Line5 = line(Label5, Formula5, axiom),

	 newKey(Label6),
	 applyMp(non(B), Formula5, Formula6),
	 Line6 = line(Label6, Formula6, mp(LabelNonB, Label5)),

	 newKey(Label7),
	 applyMp(Formula6, Formula4, Formula7),
	 Line7 = line(Label7, Formula7, mp(Label6, Label4)),

	 newKey(Label8),
	 instanceAx8(Goal, Formula8),
	 Line8 = line(Label8, Formula8, axiom),

	 newKey(Label9),
	 applyMp(non(non(Goal)), Formula8, Goal),
	 Line9 = line(Label9, Goal, mp(Label7, Label8)),

	 append(InProof, [Line1, Line2, Line3, Line4, Line5, Line6, Line7, Line8, Line9], Result)
	).

/*

  Case A & B in InProof, that is tableau T-and application: look for a conjunctive formula
  whose conjuncts are not formulas of the proof

*/

hilbertPS(InProof, Goal, Result):-
	member(line(LabelAnd, and(L, R), _), InProof),
	(
	 \+ memberchk(line(_, L, _), InProof)
	;
	 \+ memberchk(line(_, R, _), InProof)
	),
	!,

	newKey(Label1),
	instanceAx4a(L, R, Formula1),
	Line1 = line(Label1, Formula1, axiom),
	
	newKey(Label2),
	applyMp(and(L, R), Formula1, Formula2),
	Line2 = line(Label2, Formula2, mp(LabelAnd, Label1)),

	newKey(Label3),
	instanceAx4b(L, R, Formula3),
	Line3 = line(Label3, Formula3, axiom),

	newKey(Label4),
	applyMp(and(L, R), Formula3, Formula4),
	Line4 = line(Label4, Formula4, mp(LabelAnd, Label3)),

	append(InProof, [Line1, Line2, Line3, Line4], MidProof),

	hilbertPS(MidProof, Goal, Result).
		     
/*

  Modus ponens used on two formulas in InProof

*/


hilbertPS(InProof, Goal, Result):-
	member(line(LabelBImC, im(B, C), _), InProof),
	memberchk(line(LabelB, B, _), InProof),
	\+ memberchk(line(_, C, _), InProof),
	!,

	newKey(Label1),
	Line1 = line(Label1, C, mp(LabelB, LabelBImC)),

	append(InProof, [Line1], MidProof),

	hilbertPS(MidProof, Goal, Result).

/*

  Apply axiom 8 to double negated formulas in InProof

*/

hilbertPS(InProof, Goal, Result):-
	member(line(LabelNegNeg, non(non(B)), _), InProof),
	\+member(line(_,B,_),  InProof),
	!,
	newKey(Label1),
	instanceAx8(B, Formula1),
	Line1 = line(Label1, Formula1, axiom),

	newKey(Label2),
	applyMp(non(non(B)), Formula1, Formula2),
	Line2 = line(Label2, Formula2, mp(LabelNegNeg, Label1)),

	append(InProof, [Line1, Line2], MidProof),

	hilbertPS(MidProof, Goal, Result).
		     

/*

  If the negation of the goal is not in InProof (this means that the negation of the goal
  is not considered a T-formula), then we put it, thus we remember that the goal is an
  F-formula. Here we proceed according to the refined condition expressed as line 8a) in the notes.

    
*/

hilbertPS(InProof, Goal, Result):-
	goOnByContradictionExtendedCondition(InProof, Goal),
	!,
	
	newKey(LabelAss),
	LineAss = line(LabelAss, non(Goal), assumption),
	append(InProof, [LineAss], MidProof),
	
	hilbertPS(MidProof, false, MidResult),

	(
	 MidResult = model(_),
	 !,
	 Result = MidResult
	;
	 /*
	   MidResult ends with a contradictory conjunction. Both the conjuncts are in
	   MidResult
	 */

	 last(MidResult, line(_, and(K, non(K)), _)),
	 
	 dischargeAssumption(MidResult, non(Goal),  DischRes),

	 /*
	   dischargeAssumption does not guarantee that in OutProof there are ~Goal->K and
	   ~Goal->~K.
	 */

	 standardize(DischRes, non(Goal), OutProof),
	 
	 memberchk(line(LabelNonGoalImK, im(non(Goal), K), _), OutProof),
	 memberchk(line(LabelNonGoalImNonK, im(non(Goal), non(K)), _), OutProof),
	 !,

	 newKey(Label1),
	 instanceAx7(non(Goal), K, Formula1),
	 Line1 = line(Label1, Formula1, axiom),

	 newKey(Label2),
	 applyMp(im(non(Goal), K), Formula1, Formula2),
	 Line2 = line(Label2, Formula2, mp(LabelNonGoalImK, Label1)),

	 newKey(Label3),
	 applyMp(im(non(Goal), non(K)), Formula2, Formula3),
	 Line3 = line(Label3, Formula3, mp(LabelNonGoalImNonK, Label2)),

	 newKey(Label4),
	 instanceAx8(Goal, Formula4),
	 Line4 = line(Label4, Formula4, axiom),

	 newKey(Label5),
	 applyMp(Formula3, Formula4, Formula5),
	 %% note: Formula5 must be equal to Goal
	 Line5 = line(Label5, Formula5, mp(Label3, Label4)),

	 append(OutProof, [Line1, Line2, Line3, Line4, Line5], Result)
	 
	).

/*

  Goal is a  A & B:

  if we prove both A and B, then we concatenate the two proofs and three more lines are
  added. The two proofs can have lines in common, and in common with InProof. Two avoid to
  have a proof with duplicated lines, we remove them.  Note that we can have multiple
  occurrences of the same formula with different labels.  We do not care about it.

*/

hilbertPS(InProof, Goal, Result):-
	Goal = and(Left, Right),
	!,
	hilbertPS(InProof, Left, ResultLeft),
	(
	 ResultLeft = model(_),
	 !,
	 Result = ResultLeft
	;
	 hilbertPS(InProof, Right, ResultRight),
	 (
	  ResultRight = model(_),
	  !,
	  Result = ResultRight
	 ;

	  rwProof(ResultLeft, [], [], RwOfResultLeft),
	  rwProof(ResultRight, [], [], RwOfResultRight),
	  append([InProof, RwOfResultLeft, RwOfResultRight], OutProof),
	  trimProof(OutProof, [], [], TrimmedProof),
	  
	  newKey(Label1),
	  instanceAx3(Left, Right, Formula1),
	  Line1 = line(Label1, Formula1, axiom),
	  
	  newKey(Label2),
	  applyMp(Left, Formula1, Formula2),
	  getLabel(TrimmedProof, Left, LabelLeft),
	  Line2 = line(Label2, Formula2, mp(LabelLeft, Label1)),

	  newKey(Label3),
	  applyMp(Right, Formula2, Formula3),
	  getLabel(TrimmedProof, Right, LabelRight),
	  Line3 = line(Label3, Formula3, mp(LabelRight, Label2)),

	  append(TrimmedProof, [Line1, Line2, Line3], Result)
	  
	 )
	).


/*

  Goal is  B->C where B is formula in Inproof
  
*/

hilbertPS(InProof, Goal, Result):-
	Goal = im(B, C),
	getLabel(InProof, B, _),
	!,
	hilbertPS(InProof, C, MidResult),
	(
	 MidResult = model(_),
	 !,
	 Result = MidResult
	 ;
	 newKey(Label1),
	 instanceAx1a(C, B, Formula1),
	 Line1 = line(Label1, Formula1, axiom),

	 newKey(Label2),
	 applyMp(C, Formula1, Formula2),
	 getLabel(MidResult, C, LabelOfC),
	 Line2 = line(Label2, Formula2, mp(LabelOfC, Label1)),

	 append(MidResult, [Line1, Line2], Result)
	 ).

/*

  Case Goal = B -> C
  
*/

hilbertPS(InProof, Goal, Result):-
	Goal = im(B, C),
	!,
	newKey(Label1),
	Line1 = line(Label1, B, assumption),
	append(InProof, [Line1], InProofNew),
	hilbertPS(InProofNew, C, MidResult),
	(
	 MidResult = model(_),
	 !,
	 Result = MidResult
	;
	 dischargeAssumption(MidResult, B,  Result)
	).

/*

  Case Goal = neg B

*/

hilbertPS(InProof, Goal, Result):-
	Goal = non(B),
	!,
	newKey(Label1),
	Line1 = line(Label1, B, assumption),
	append(InProof, [Line1], InProofNew),
	hilbertPS(InProofNew, false, MidResult),
	(
	 MidResult = model(_),
	 !,
	 Result = MidResult
	;
	 
	 last(MidResult, line(_, and(K, non(K)), _)),

	 dischargeAssumption(MidResult, B,  OutProof),

	 standardize(OutProof, B, MidResult2),
	 
	 memberchk(line(LabelK, im(B, K), _), MidResult2),
	 memberchk(line(LabelNonK, im(B, non(K)),_), MidResult2),
	 !,
	 newKey(Label2),
	 instanceAx7(B, K, Formula2),
	 Line2 = line(Label2, Formula2, axiom),

	 newKey(Label3),
	 applyMp(im(B, K), Formula2, Formula3),
	 Line3 = line(Label3, Formula3, mp(LabelK, Label2)),

	 newKey(Label4),
	 applyMp(im(B, non(K)), Formula3, Formula4),
	 Line4 = line(Label4, Formula4, mp(LabelNonK, Label3)),
	 append(MidResult2, [Line2, Line3, Line4], Result)
	).
	       

/*

  Case Goal = B1 | B2

*/

hilbertPS(InProof, Goal, Result):-
	Goal = or(_, _),
	!,
	(
	 firstCaseOrGoal(InProof, Goal, Result),
	 !
	;
	 secondCaseOrGoal(InProof, Goal, Result),
	 !
	;
	 thirdCaseOrGoal(InProof, Goal, Result)
	).



/*

  Case B1 | B2 in InProof and Goal is false
  
*/

hilbertPS(InProof, Goal, Result):-
	Goal = false,
	member(line(_, or(B1, B2), _), InProof),
	\+ memberchk(line(_, B1, _), InProof),
	\+ memberchk(line(_, B2, _), InProof),
	!,
	
	newKey(LabelB1),
	LineB1Ass = line(LabelB1, B1, assumption),
	append(InProof, [LineB1Ass], InProofB1Ass),
	hilbertPS(InProofB1Ass, false, ResultB1),
	(
	 ResultB1 = model(_),
	 !,
	 Result = ResultB1
	;
	 newKey(LabelB2),
	 LineB2Ass = line(LabelB2, B2, assumption),
	 append(InProof, [LineB2Ass], InProofB2Ass),
	 hilbertPS(InProofB2Ass, false, ResultB2),
	 (
	  ResultB2 = model(_),
	  !,
	  Result = ResultB2
	  ;
	  /*

	    By contruction ResultB1 and ResultB2 are proofs ending with a contradictory
	    conjunction. We have the problem of getting the same conclusion for both.
	    
	  */
	  
	  appendProofOfNegation(ResultB1, or(B1, B2), B1ProofAppend),
	  dischargeAssumption(B1ProofAppend, B1, ResultB1ImNeg),
	  rwProof(ResultB1ImNeg, [], [], RwOfResultB1ImNeg),

	  appendProofOfNegation(ResultB2, or(B1, B2), B2ProofAppend),
	  dischargeAssumption(B2ProofAppend, B2, ResultB2ImNeg),
	  rwProof(ResultB2ImNeg, [], [], RwOfResultB2ImNeg),

	  append([InProof, RwOfResultB1ImNeg, RwOfResultB2ImNeg], OutProof),
	  trimProof(OutProof, [],[], OutProofTrimmed),
	  
	  orElimination(OutProofTrimmed, or(B1, B2), non(or(B1, B2)), ToFixResult),
	  addContradictoryConjunction(ToFixResult, or(B1, B2), Result)
	 )
	).


/*

  Case B1 | B2 in InProof and Goal is atomic formula
  
*/

hilbertPS(InProof, Goal, Result):-
	\+ Goal = false,
	member(line(_, or(B1, B2), _), InProof),
	\+ memberchk(line(_, B1, _), InProof),
	\+ memberchk(line(_, B2, _), InProof),
	!,
	newKey(LabelB1),
	LineB1Ass = line(LabelB1, B1, assumption),
	append(InProof, [LineB1Ass], InProofB1Ass),
	hilbertPS(InProofB1Ass, Goal, ResultB1),
	(
	 ResultB1 = model(_),
	 !,
	 Result = ResultB1
	;
	 newKey(LabelB2),
	 LineB2Ass = line(LabelB2, B2, assumption),
	 append(InProof, [LineB2Ass], InProofB2Ass),
	 hilbertPS(InProofB2Ass, Goal, ResultB2),
	 (
	  ResultB2 = model(_),
	  !,
	  Result = ResultB2
	  ;
	  dischargeAssumption(ResultB1, B1, ResultB1ImGoal),
	  rwProof(ResultB1ImGoal, [], [], RwOfResultB1ImGoal),
	 
	  dischargeAssumption(ResultB2, B2, ResultB2ImGoal),
	  rwProof(ResultB2ImGoal, [], [], RwOfResultB2ImGoal),

	  append([InProof, RwOfResultB1ImGoal, RwOfResultB2ImGoal], OutProof),
	  trimProof(OutProof, [],[], OutProofTrimmed),

	  orElimination(OutProofTrimmed, or(B1, B2), Goal, Result)
	  )
	 ).


/*

  Case not (B1 | B2) is in InProof  (Goal is an atomic formula or false)
  
*/

hilbertPS(InProof, Goal, Result):-
	member(line(_, non(or(B1, B2)), _), InProof),
	(
	 \+ memberchk(line(_, non(B1), _), InProof)
	;
	 \+ memberchk(line(_, non(B2), _), InProof)
	),
	!,
	hilbertPS(InProof, or(B1, B2), MidResult),
	(
	 MidResult = model(_),
	 !,
	 Result = MidResult
	;
	 rwProof(MidResult, [], [], MidProof),
	 append(InProof, MidProof, ToTrimProof),
	 trimProof(ToTrimProof, [], [], TrimmedProof),
	 (

	  Goal = false,
	  !,
	  /* conclude the proof with a contradictory conjunction */

	  addContradictoryConjunction(TrimmedProof, or(B1, B2), Result)
	 ;
	  
	  /* last line of TrimmedProof contains one of the complementary formulas */
	  deduceFromContradiction(TrimmedProof, Goal, Result)
	 )
	).


/*

  Case not (B1 -> B2) is in InProof  (Goal is an atomic formula or false)
  
*/

hilbertPS(InProof, Goal, Result):-
	member(line(_, non(im(B1, B2)), _), InProof),
	(
	 \+ memberchk(line(_, B1, _), InProof)
	;
	 \+ memberchk(line(_, non(B2), _), InProof)
	),
	!,
	hilbertPS(InProof, im(B1, B2), MidResult),
	(
	 MidResult = model(_),
	 !,
	 Result = MidResult
	;
	 rwProof(MidResult, [], [], MidProof),
	 append(InProof, MidProof, ToTrimProof),
	 trimProof(ToTrimProof, [], [], TrimmedProof),
	 (
	  Goal = false,
	  !,
	  addContradictoryConjunction(TrimmedProof, im(B1, B2), Result)
	 ;

	  /* last line of TrimmedProof contains one of the complementary formulas */
	  deduceFromContradiction(TrimmedProof, Goal, Result)
	 )
	).


/*

  Case not (B1 & B2) is in InProof  (Goal is an atomic formula or false)
  
*/

hilbertPS(InProof, Goal, Result):-
	member(line(_, non(and(B1, B2)), _), InProof),
	\+ memberchk(line(_, non(B1), _), InProof),
	\+ memberchk(line(_, non(B2), _), InProof),
	!,

	hilbertPS(InProof, and(B1, B2), MidResult),
	(
	 MidResult = model(_),
	 !,
	 Result = MidResult
	;
	 rwProof(MidResult, [], [], MidProof),
	 append(InProof, MidProof, ToTrimProof),
	 trimProof(ToTrimProof, [], [], TrimmedProof),
	 (
	  Goal = false,
	  !,
	  addContradictoryConjunction(TrimmedProof, and(B1, B2), Result)
	 ;

	  /* last line of TrimmedProof contains one of the complementary formulas */
	  deduceFromContradiction(TrimmedProof, Goal, Result)
	 )
	).


/*

  Case B -> C is in InProof  (Goal is an atomic formula or false)
  
*/

hilbertPS(InProof, Goal, Result):-
	member(line(_, im(B,C), _), InProof),
	\+ isAxiom(im(B,C), _),
	\+ memberchk(line(_, non(B), _), InProof),
	\+ memberchk(line(_, C, _), InProof),
	!,
	hilbertPS(InProof, B, ResultOnB),
	(
	 ResultOnB = model(_),
	 !,
	 Result = ResultOnB
	;
	 append(InProof, ResultOnB, ProofSequence),
	 rwProof(ProofSequence, [], [], GoodProofOfB),
	 trimProof(GoodProofOfB, [], [], TrimmedProofOfB),

	 getLabel(TrimmedProofOfB, im(B,C), LabelBImC),
	 getLabel(TrimmedProofOfB, B, LabelB),

	 newKey(Label1),
	 Line1 = line(Label1, C, mp(LabelB, LabelBImC)),

	 append(TrimmedProofOfB, [Line1], ProofOfC),

	 hilbertPS(ProofOfC, Goal, Result)
	).



/*

  base case: no rule is applicable, thus the goal is not provable. We return a classical
  propositional model. The model satisfied all the formulas in InProof and does not
  satisfies the goal.

*/

hilbertPS(InProof, _, model(Atoms)):-
	getAtoms(InProof, Atoms). 
	
	



firstCaseOrGoal(InProof, Goal, Result):-
	Goal = or(B1, _),
	\+ getLabel(InProof, non(B1), _),
	hilbertPS(InProof, B1, MidResult),
	(
	 MidResult = model(_),
	 !,
	 Result = MidResult
	 ;
	 newKey(Label1),
	 instanceAx5a(Goal, Formula1),
	 Line1 = line(Label1, Formula1, axiom),

	 newKey(Label2),
	 applyMp(B1, Formula1, Formula2),
	 getLabel(MidResult, B1, LabelB1),
	 Line2 = line(Label2, Formula2, mp(LabelB1 , Label1)),
	 
	 append(MidResult, [Line1, Line2], Result)
	 ).


secondCaseOrGoal(InProof, Goal, Result):-
	Goal = or(_, B2),
	\+ getLabel(InProof, non(B2), _),
	hilbertPS(InProof, B2, MidResult),
	(
	 MidResult = model(_),
	 !,
	 Result = MidResult
	 ;
	 newKey(Label1),
	 instanceAx5b(Goal, Formula1),
	 Line1 = line(Label1, Formula1, axiom),

	 newKey(Label2),
	 applyMp(B2, Formula1, Formula2),
	 getLabel(MidResult, B2, LabelB2),
	 Line2 = line(Label2, Formula2, mp(LabelB2 , Label1)),

	 append(MidResult, [Line1, Line2], Result)
	).


thirdCaseOrGoal(InProof, Goal, Result):-
	hilbertPS(InProof, false, MidResult),
	(
	 MidResult = model(_),
	 !,
	 Result = MidResult
	;
	 /*
	   by contruction MidResult ends with a contradictory conjunction
	 */
	 
	 last(MidResult, line(_, and(K, non(K)), _)),
	 memberchk(line(LabelK, K, _), MidResult),
	 memberchk(line(LabelNonK, non(K), _), MidResult),
	 !,
	 newKey(Label1),
	 instanceAx7(non(Goal), K, Formula1),
	 Line1 = line(Label1, Formula1, axiom),

	 newKey(Label2),
	 instanceAx1a(K, non(Goal), Formula2),
	 Line2 = line(Label2, Formula2, axiom),

	 newKey(Label3),
	 applyMp(K, Formula2, Formula3),
	 Line3 = line(Label3, Formula3, mp(LabelK, Label2)),

	 newKey(Label4),
	 applyMp(Formula3, Formula1, Formula4),
	 Line4 = line(Label4, Formula4, mp(Label3, Label1)),

	 newKey(Label5),
	 instanceAx1a(non(K), non(Goal), Formula5),
	 Line5 = line(Label5, Formula5, axiom),

	 newKey(Label6),
	 applyMp(non(K), Formula5, Formula6),
	 Line6 = line(Label6, Formula6, mp(LabelNonK, Label5)),

	 newKey(Label7),
	 applyMp(Formula6, Formula4, Formula7),
	 Line7 = line(Label7, Formula7, mp(Label6, Label4)),

	 newKey(Label8),
	 instanceAx8(Goal, Formula8),
	 Line8 = line(Label8, Formula8, axiom),

	 newKey(Label9),
	 applyMp(Formula7, Formula8, Formula9),
	 Line9 = line(Label9, Formula9, mp(Label7, Label8)),

	 append(MidResult, [Line1, Line2, Line3, Line4, Line5, Line6, Line7, Line8, Line9], Result)	 
	 
	).


