:-use_module(proofSearch).
:-use_module(classicalAxioms).




runTest(InProof, Goal, Result):-
	hilbertPS(InProof, Goal, OutProof),
	(
	 OutProof = model(M),
	 !,
	 writeln('Found a countermodel. List of the propositional variables:'),
	 writeln(M)
	;
	 eraseUnusedLines(OutProof, Result),
	 isHilbertProof(Result, []),
	 prettyPrint(Result,[], 1)
	).

prepareInProof([], [], _).
prepareInProof([ Formula | Tail], InProof, LN):-
	NextLN is LN + 1,
	prepareInProof(Tail, TailProof, NextLN),
	InProof = [line(LN, Formula, assumption) | TailProof].


buildEquiv(X, Y, Z):-
	Z = and(im(X, Y), im(Y, X)).




/*
  test dischargeAssumption

*/

try:-
	Line1 = line(1, b, assumption),
	Line2 = line(2, im(b, b), assumption),

	Line4 = line(4, im(b,c), assumption),
	Line5 = line(5, c, mp(1,4)),
	InProof = [Line1, Line2,  Line4, Line5],
	isHilbertProof(InProof, []),
	dischargeAssumption(InProof, b, Result),
	writeln('inProof'),
	printproof(InProof),
	isHilbertProof(Result, []),
	printproof(Result).



/* iM<number> are formulas given in Kleene IM 1952*/

p1:-
	Goal = im(non(im(p,q)) ,p),
	runTest([], Goal, _).



iM1:-
	Goal = im(a, a),
	runTest([], Goal, _).

iM2:-
	Line1 = line(1, im(a,b), assumption),
	Line2 = line(2, im(b,c), assumption),
	InProof = [Line1, Line2],
	Goal = im(a,c),
	runTest(InProof, Goal, _).

iM6:-
	Line1 = line(1, im(a, b), assumption),
	InProof = [Line1],
	X = im(b, c),
	Y = im(a, c),
	Goal = im(X, Y),
	runTest(InProof, Goal, _).

iM10a:-
	Line1 = line(1, non(a), assumption),
	InProof = [Line1],
	Goal = im(a, b),
	runTest(InProof, Goal, _).

iM10b:-
	Line1 = line(1, a, assumption),
	InProof = [Line1],
	Goal = im(non(a), b),
	runTest(InProof, Goal, _).

iM11:-
	Line1 = line(1, b, assumption),
	InProof = [Line1],
	Goal = im(a, b),
	runTest(InProof, Goal, _).

iM12:-
	prepareInProof([im(a,b)], InProof, 1),
	Goal = im(non(b), non(a)),
	runTest(InProof, Goal, _).

iM13:-
	prepareInProof([im(a,non(b))], InProof, 1),
	Goal = im(b, non(a)),
	runTest(InProof, Goal, _).

iM14:-
	prepareInProof([im(non(a), b)], InProof,1),
	Goal = im(non(b), a),
	runTest(InProof, Goal, _).

iM15:-
	prepareInProof([im(non(a), non(b))], InProof,1),
	Goal = im(b, a),
	runTest(InProof, Goal, _).

iM22:-
	prepareInProof([im(a, im(b,c)), non(non(a)), non(non(b))], InProof, 1),
	Goal = non(non(c)),
	runTest(InProof, Goal, _).

iM23:-
	prepareInProof([non(non(im(a, b)))], InProof, 1),
	Goal = im(non(non(a)), non(non(b))),
	runTest(InProof, Goal, _).

iM24:-
	prepareInProof([non(non(im(a, b))), non(non(im(b,c)))], InProof, 1),
	Goal = non(non(im(a, c))),
	runTest(InProof, Goal, _).


iM49a:-
	Goal = im(a, non(non(a))),
	runTest([], Goal, _).

iM49b:-
	Goal = im(non(a), non(non(non(a)))),
	runTest([], Goal, _).



iM49c:-
	X = or(a, non(a)),
	Y = im(non(non(a)), a),
	Goal = im(X, Y),
	runTest([], Goal, _).


iM49c1:-
	X = or(a, non(a)),
	buildEquiv(non(non(a)), a, Y),
	Goal = im(X, Y),
	runTest([], Goal, _).



iM50a:-
	X = a,
	Y = non(a),
	buildEquiv(X, Y, Mid),
	Goal = non(Mid),
	runTest([], Goal, _).

iM51a:-
	Goal = non(non(or(a, non(a)))),
	runTest([], Goal, _).


iM56:-
	X = or(a, b),
	Y = non(and(non(a), non(b))),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).

iM56a:-
	X = or(a,b),
	Y = non(and(non(a), non(b))),
	Goal = im(X, Y),
	runTest([], Goal, _).

iM56b:-
	X = or(non(a), b),
	Y = non(and(a,non(b))),
	Goal = im(X, Y),
	runTest([], Goal, _).



iM57:-
	X = and(a,b),
	Y = non(or(non(a) ,non(b) )),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).

iM57XtoY:-
	X = and(a,b),
	Y = non(or(non(a) ,non(b) )),
	runTest([], im(X, Y), _).

iM57YtoX:-
	X = and(a,b),
	Y = non(or(non(a) ,non(b) )),
	runTest([], im(Y, X), _).


iM57b:-
	X = and(a, non(b)),
	Y = non(or(non(a),b)),
	Goal = im(X, Y),
	runTest([], Goal, _).

iM58:-
	X = im(a,b),
	Y = non(and(a, non(b))),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).

iM58b:-
	X = im(a, non(b)),
	Y = non(and(a, b)),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).

iM58c:-
	X = non(and(a, b)),
	Y = im(non(non(a)), non(b)),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).


iM58d:-
	X = im(non(non(a)), non(b)),
	Y = non(non(or(non(a), non(b)))),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).

iM58e:-
	prepareInProof([im(non(non(b)), b)], InProof, 1),
	X = im(non(non(a)), b),
	Y = im(a, b),
	buildEquiv(X, Y, Goal),
	runTest(InProof, Goal, _).


iM58f:-
	prepareInProof([im(non(non(b)), b)], InProof, 1),
	X = im(a, b),
	Y = non(and(a,non(b))),
	buildEquiv(X, Y, Goal),
	runTest(InProof, Goal, _).

iM58g:-
	X = im(non(non(a)), b),
	Y = non(and(a, non(b))),
	Goal = im(X, Y),
	runTest([], Goal, _).


iM59:-
	X = im(a,b),
	Y = or(non(a), b),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).

iM59b:-
	X = im(a,b),
	Y = non(non(or(non(a), b))),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).

iM59c:-
	X = im(non(a), b),
	Y = non(non(or(a, b))),
	Goal = im(X, Y),
	runTest([], Goal, _).

iM60:-
	X = and(a, b),
	Y = non(im(a, non(b))),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).


iM60b:-
	X = and(a, non(b)),
	Y = non(im(a, b)),
	Goal = im(X, Y),
	runTest([], Goal, _).


iM60c:-
	X = and(non(non(a)), b),
	Y = non(im(a, non(b))),
	Goal = im(X, Y),
	runTest([], Goal, _).

iM60d:-
	X = and(non(non(a)), non(b)),
	Y = non(im(a, b)),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).

iM60e:-
	X = non(im(a, b)),
	Y = non(or(non(a),b)),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).

iM60f:-
	X = non(or(non(a),b)),
	Y = non(non(and(a, non(b)))),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).


iM60g:-
	X = non(non(im(a, b))),
	Y = non(and(a, non(b))),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).

iM60h:-
	X = non(and(a, non(b))),
	Y = im(a, non(non(b))),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).

iM60i:-
	X = im(a, non(non(b))),
	Y = im(non(non(a)), non(non(b))),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).


iM61:-
	X = or(a, b),
	Y = im(non(a), b),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).

iM61b:-
	X = non(or(a, b)),
	Y = non(im(non(a), b)),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).


iM62:-
	X = non(and(a, b)),
	Y = or(non(a), non(b)),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).

iM63:-
	X = non(or(a, b)),
	Y = and(non(a), non(b)),
	buildEquiv(X, Y, Goal),
	runTest([], Goal, _).






/* simple checks */

testNonNon:-
	InProof = [line(1, non(non(p)), assumption)],
	Goal = q,
	runTest(InProof, Goal, _).


testAnd:-
	Line1 = line(1, and(p,q), assumption),
	Line2 = line(2, im(and(p,q), p), axiom),
	Line3 = line(3, p, mp(1,2)),
	Line4 = line(4, im(and(p,q), q), axiom),
	Line5 = line(5, q, mp(1,4)),
	Line6 = line(6, non(and(q,p)), assumption),
	InProof = [Line1, Line2, Line3, Line4, Line5, Line6],

	Goal = and(q,p),

	runTest(InProof, Goal, _).



testGoalIm1:-
	Line1 = line(1, non(im(b,c)), assumption),
	Line2 = line(2, b, assumption),
	Line3 = line(3, c, assumption),
	InProof = [Line1, Line2, Line3],

	Goal = im(b,c),

	runTest(InProof, Goal, _).


testGoalIm2:-
	Line1 = line(1, non(im(b,c)), assumption),
	Line3 = line(3, c, assumption),
	InProof = [Line1, Line3],

	Goal = im(b,c),

	runTest(InProof, Goal, _).


testNon:-
	Line1 = line(1, and(p,q), assumption),
	Line2 = line(2, im(and(p,q), p), axiom),
	Line3 = line(3, p, mp(1,2)),
	Line4 = line(4, im(and(p,q), q), axiom),
	Line5 = line(5, q, mp(1,4)),
	InProof = [Line1, Line2, Line3, Line4, Line5],

	Goal = and(q,p),

	runTest(InProof, Goal, _).


testThirdCase:-
	Line1 = line(1, p, assumption),
	Line2 = line(2, non(p), assumption),
	InProof = [Line1, Line2],

	Goal = q,

	runTest(InProof, Goal, _).

testOrGoal:-
	Line1 = line(1, non(or(p,q)), assumption),
	Line2 = line(2, q, assumption),
	Line3 = line(3, non(p), assumption),
	InProof = [Line1, Line2, Line3],

	Goal = or(p,q),

	runTest(InProof, Goal, _).


testOrProof:-
	Line1 = line(1, or(p,q), assumption),
	Line2 = line(2, non(p), assumption),
	Line3 = line(3, non(q), assumption),
	InProof = [Line1, Line2, Line3],

	Goal = false,

	runTest(InProof, Goal, _).


testOrProof2:-
	Line1 = line(1, or(and(p,a),and(q,a)), assumption),
	Line2 = line(2, non(a), assumption),
	InProof = [Line1, Line2],

	Goal = a,

	runTest(InProof, Goal, _).


testNonOrProofGoalFalse:-
	Line1 = line(1, q, assumption),
	Line2 = line(2, non(or(p, q)), assumption),
	InProof = [ Line1, Line2],

	Goal = false,

	runTest(InProof, Goal, _).

testNonOrProofGoalAtom:-
	Line1 = line(1, q, assumption),
	Line2 = line(2, non(or(p, q)), assumption),
	Line3 = line(3, non(a), assumption),
	InProof = [ Line1, Line2, Line3],

	Goal = a,

	runTest(InProof, Goal, _).


testNonImProofGoalFalse:-
	Line1 = line(1, q, assumption),
	Line2 = line(2, non(im(p, q)), assumption),
	InProof = [ Line1, Line2],

	Goal = false,

	runTest(InProof, Goal, _).


testNonImProofGoalAtom:-
	Line1 = line(1, q, assumption),
	Line2 = line(2, non(im(p, q)), assumption),
	Line3 = line(3, non(a), assumption),
	InProof = [ Line1, Line2, Line3],

	Goal = a,

	runTest(InProof, Goal, _).


testNonAndProofGoalFalse:-
	Line1 = line(1, q, assumption),
	Line2 = line(2, non(and(p, q)), assumption),
	Line3 = line(3, p, assumption),
	InProof = [ Line1, Line2, Line3],

	Goal = false,

	runTest(InProof, Goal, _).


testNonAndProofGoalAtom:-
	Line1 = line(1, q, assumption),
	Line2 = line(2, non(and(p, q)), assumption),
	Line3 = line(3, p, assumption),
	Line4 = line(4, non(a), assumption),
	InProof = [ Line1, Line2, Line3, Line4],

	Goal = a,

	runTest(InProof, Goal, _).

testModusPonens:-
	Line0 = line(0, im(a,b), assumption),
	Line1 = line(1, im(p,q), assumption),
	Line2 = line(2, p, assumption),
	Line3 = line(3, a, assumption),
	Line4 = line(4, b, assumption),
	InProof = [Line0, Line1, Line2, Line3, Line4],

	Goal = q,

	runTest(InProof, Goal, _).


test1:-
	Goal = im(and(p, q), and(q,p)),
	InProof = [],
	runTest(InProof, Goal, _).


test2:-
	Goal = or(a, non(a)),
	InProof = [],
	runTest(InProof, Goal, _).


test3:-
	A = non(non(p)),
	B = q,
	C = non(or(A, B)),
	Line1 = line(1, C, assumption),
	InProof = [Line1],

	Goal = false,
	runTest(InProof, Goal, _).



test4:-
	A = non(p),
	B = non(q),
	C = non(or(A, B)),
	D = and(non(A), non(B)),
	Goal = im(C, D),

	InProof = [],

	runTest(InProof, Goal, _).

tmpNonNon:-
	InProof = [line(1, non(non(p)), assumption)],
	Goal = q,
	hilbertPS(InProof, Goal, Result),
	writeln(Result).

tmpAnd:-
	Line1 = line(1, and(p,q), assumption),
	Line2 = line(2, im(and(p,q), p), axiom),
	Line3 = line(3, p, mp(1,2)),
	Line4 = line(4, im(and(p,q), q), axiom),
	Line5 = line(5, q, mp(1,4)),
	Line6 = line(6, non(and(q,p)), assumption),
	InProof = [Line1, Line2, Line3, Line4, Line5, Line6],
	Goal = and(q,p),
	hilbertPS(InProof, Goal, Result),
	printproof(Result).

tmpGoalIm1:-
	Line1 = line(1, non(im(b,c)), assumption),
	Line2 = line(2, b, assumption),
	Line3 = line(3, c, assumption),
	InProof = [Line1, Line2, Line3],
	Goal = im(b,c),
	hilbertPS(InProof, Goal, Result),
	printproof(Result).

tmpGoalIm2:-
	Line1 = line(1, non(im(b,c)), assumption),
	Line3 = line(3, c, assumption),
	InProof = [Line1, Line3],
	Goal = im(b,c),
	hilbertPS(InProof, Goal, Result),
	printproof(Result).

tmpNon:-
	Line1 = line(1, and(p,q), assumption),
	Line2 = line(2, im(and(p,q), p), axiom),
	Line3 = line(3, p, mp(1,2)),
	Line4 = line(4, im(and(p,q), q), axiom),
	Line5 = line(5, q, mp(1,4)),
	InProof = [Line1, Line2, Line3, Line4, Line5],
	Goal = and(q,p),
	hilbertPS(InProof, Goal, Result),
	printproof(Result).

tmpThirdCase:-
	Line1 = line(1, p, assumption),
	Line2 = line(2, non(p), assumption),
	Goal = q,
	InProof = [Line1, Line2],
	thirdCaseOrGoal(InProof, Goal, Result),
	printproof(Result).

tmpOrGoal:-
	Line1 = line(1, non(or(p,q)), assumption),
	Line2 = line(2, q, assumption),
	Line3 = line(3, non(p), assumption),

	Goal = or(p,q),
	InProof = [Line1, Line2, Line3],
	hilbertPS(InProof, Goal, Result),
	printproof(Result).

tmpOrProof:-
	Line1 = line(1, or(p,q), assumption),
	Line2 = line(2, non(p), assumption),
	Line3 = line(3, non(q), assumption),
	Goal = false,
	InProof = [Line1, Line2, Line3],
	hilbertPS(InProof, Goal, Result),
	printproof(Result).

tmpOrProof2:-
	Line1 = line(1, or(and(p,a),and(q,a)), assumption),
	Line2 = line(2, non(a), assumption),
	Goal = a,
	InProof = [Line1, Line2],
	hilbertPS(InProof, Goal, Result),
	printproof(Result).


tmpNonOrProofGoalFalse:-
	Line1 = line(1, q, assumption),
	Line2 = line(2, non(or(p, q)), assumption),
	Goal = false,
	InProof = [ Line1, Line2],
	hilbertPS(InProof, Goal, Result),
	isHilbertProof(Result, []),
	printproof(Result).

tmpNonOrProofGoalAtom:-
	Line1 = line(1, q, assumption),
	Line2 = line(2, non(or(p, q)), assumption),
	Line3 = line(3, non(a), assumption),
	Goal = a,
	InProof = [ Line1, Line2, Line3],
	hilbertPS(InProof, Goal, Result),
	isHilbertProof(Result, []),
	printproof(Result).


tmpNonImProofGoalFalse:-
	Line1 = line(1, q, assumption),
	Line2 = line(2, non(im(p, q)), assumption),
	Goal = false,
	InProof = [ Line1, Line2],
	hilbertPS(InProof, Goal, Result),
	isHilbertProof(Result, []),
	printproof(Result).

tmpNonImProofGoalAtom:-
	Line1 = line(1, q, assumption),
	Line2 = line(2, non(im(p, q)), assumption),
	Line3 = line(3, non(a), assumption),
	Goal = a,
	InProof = [ Line1, Line2, Line3],
	hilbertPS(InProof, Goal, Result),
	isHilbertProof(Result, []),
	printproof(Result).


tmpNonAndProofGoalFalse:-
	Line1 = line(1, q, assumption),
	Line2 = line(2, non(and(p, q)), assumption),
	Line3 = line(3, p, assumption),
	Goal = false,
	InProof = [ Line1, Line2, Line3],
	hilbertPS(InProof, Goal, Result),
	isHilbertProof(Result, []),
	printproof(Result).

tmpNonAndProofGoalAtom:-
	Line1 = line(1, q, assumption),
	Line2 = line(2, non(and(p, q)), assumption),
	Line3 = line(3, p, assumption),
	Line4 = line(4, non(a), assumption),
	Goal = a,
	InProof = [ Line1, Line2, Line3, Line4],
	hilbertPS(InProof, Goal, Result),
	isHilbertProof(Result, []),
	printproof(Result).

tmpModusPonens:-
	Line0 = line(0, im(a,b), assumption),
	Line1 = line(1, im(p,q), assumption),
	Line2 = line(2, p, assumption),
	Line3 = line(3, a, assumption),
	Line4 = line(4, b, assumption),

	Goal = q,

	InProof = [Line0, Line1, Line2, Line3, Line4],
	hilbertPS(InProof, Goal, Result),
	isHilbertProof(Result, []),
	printproof(Result).

tmp1:-
	Goal = im(and(p, q), and(q,p)),
	hilbertPS([], Goal, Result),
	isHilbertProof(Result, []),
	printproof(Result).

tmp2:-
	Goal = or(a, non(a)),
	hilbertPS([], Goal, Result),
	isHilbertProof(Result, []),
	printproof(Result).

tmp3:-
	A = non(non(p)),
	B = q,
	C = non(or(A, B)),

	Line1 = line(1, C, assumption),
	Goal = false,
	InProof = [Line1],
	hilbertPS(InProof, Goal, Result),
	printproof(Result).


tmp4:-
	A = non(p),
	B = non(q),
	C = non(or(A, B)),
	D = and(non(A), non(B)),

	Goal = im(C, D),
	InProof = [],
	hilbertPS(InProof, Goal, Result),
	printproof(Result).



nagata3:-
	Goal = or(p3,im(p3,or(p2,im(p2,or(p1,im(p1,or(p0,non(p0)))))))),
	runTest([], Goal, _).


intDecide(Formula, _, Label):-
	hilbertPS([], Formula, Result),
	write(Label),
	(
	 Result = model(_),
	 !,
	 writeln('unprovable')
	;
	 writeln('provable')
	).
