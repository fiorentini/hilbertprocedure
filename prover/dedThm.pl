:-module(deductionTheorem, [dischargeAssumption/3]).

:-use_module(classicalAxioms).



/*
  Given the proof in the first argument, discharge the formula assumption given in the
  second argument. Third argument is the proof already fixed,. Fourth argument is the
  resulting proof.  The relative order of the lines in the first argument is preserved.
  The formula in the last line of Result is DischargingAssumption->H, where H is the
  formula occurring in the last line of InProof.
  
  Note that some assumptions of the inProof could not occurr in the Resulting proof.
  


*/

dischargeAssumption(InProof, DischargingAssumption, Result):-
	dischargeAssumptionHelper(InProof, DischargingAssumption, [], OutProof),
	/*

	  Check that che conclusion of OutProof is of the kind DischargingAssumption->H,
	  where H is the conclusion of InProof

	*/

	last(InProof, InProofLastLine),
	InProofLastLine = line(_, InProofConclusion, _),

	last(OutProof, OutProofLastLine),
	OutProofLastLine = line(KeyConclusion, OutProofConclusion, _),
	
	(
	 OutProofConclusion = im(DischargingAssumption, InProofConclusion),
	 !,
	 Result = OutProof
	;
	 InProofConclusion = OutProofConclusion,
	 !,
	 newKey(NewKey1),
	 Line1 = line(NewKey1,
		      im(OutProofConclusion, im(DischargingAssumption, OutProofConclusion)),
		      axiom
		     ),
	 
	 newKey(NewKey2),
	 Line2 = line(NewKey2,
		      im(DischargingAssumption, OutProofConclusion),
		      mp(KeyConclusion, NewKey1)
		     ),
	 append(OutProof, [Line1, Line2], Result)
	;
	 writeln('error in dischargeAssumption'),
	 abort
	).
	

/*
  Base case: no proof line to fix.

*/

dischargeAssumptionHelper([], _, FixedProof, FixedProof).

/*
  Fix line H. To do this we need the already FixedProof
*/

dischargeAssumptionHelper([ H | T], DischargingAssumption, FixedProof, ProofResult):-
	deductionTheorem(H, DischargingAssumption, FixedProof, FixedProofForH),
	dischargeAssumptionHelper(T, DischargingAssumption, FixedProofForH, ProofResult).
	
/*

  The proof-line is the assumption to be discharged
  
*/

deductionTheorem(line(Key, DischAss, assumption), DischAss, FixedProof, ProofLines):-
	!,
	newKey(NewKey1),
	newKey(NewKey2),
	newKey(NewKey3),
	newKey(NewKey4),
	Ax1a = im(DischAss, im(DischAss, DischAss)),
	Line1 = line(NewKey1, Ax1a, axiom),
	Ax1b = im(DischAss, im(im(DischAss, DischAss), DischAss)),


	Line2 = line(NewKey2, Ax1b, axiom),
	Ax1b2 = im(Ax1a, im(Ax1b, im(DischAss, DischAss))),
	

	Line3 = line(NewKey3, Ax1b2, axiom),
	Line4 = line(NewKey4, im(Ax1b, im(DischAss, DischAss)), mp(NewKey1, NewKey3)),
	Line5 = line(Key, im(DischAss, DischAss), mp(NewKey2, NewKey4)),
	append(FixedProof, [Line1, Line2, Line3, Line4, Line5], ProofLines).
		     
	
/*

  The proof-line is an assumpition different to the one to be discharged or an axiom: we
  do not do any change.

*/

deductionTheorem(line(Key, Formula, Type), _, FixedProof, ProofLines):-
	(
	 Type = assumption
	 ;
	 Type = axiom
	),
	!,
	append(FixedProof, [line(Key, Formula, Type)], ProofLines).

/*

  The proof-line is a modus ponens application 

*/

deductionTheorem(
		 MpLine,
		 DischargingAssumption,
		 FixedProof,
		 ProofLines
		):-

	/* get the components of the line to be rewritten */
	MpLine = line(Key, MpConclusion, mp(LeftRef, RightRef)),

	/* left premise of Mp application */
	getLine(FixedProof, LeftRef, LineOfLeftPrem),
	LineOfLeftPrem = line(LeftRef, LeftFormulaPrem, _),

	/* right premise of Mp application */
	getLine(FixedProof, RightRef, LineOfRightPrem),
	LineOfRightPrem = line(RightRef, RightFormulaPrem, _),

	(
	 /* left and right Mp  premises have not been rewritten */ 
	 nothingToDo(LeftFormulaPrem, RightFormulaPrem, MpConclusion),
	 !,
	 append(FixedProof, [line(Key, MpConclusion, mp(LeftRef, RightRef))], ProofLines)
	 ;

	 /* Mp conclusion have to be rewritten but it coincides with an axiom */
	 Target = im(DischargingAssumption, MpConclusion),
	 isAxiom(Target, _),
	 !,
	 Line1 = line(Key, Target, axiom),
	 append(FixedProof, [Line1], ProofLines)
	;

	 /* both the Mp premises have been rewritten. We rewrite Mp conclusion as usual */
	 bothPremisesAreChanged(LeftFormulaPrem, RightFormulaPrem, DischargingAssumption,
				MpConclusion),
	 !,
	 (
	  LeftFormulaPrem = im(DischargingAssumption, OriginalLeftPremise),
	  RightFormulaPrem = im(DischargingAssumption, OriginalRightPremise),
	  OriginalRightPremise = im(OriginalLeftPremise, ConsequentOfOriginalRightPremise),
	  instanceAx1b(
		       DischargingAssumption,
		       OriginalLeftPremise,
		       ConsequentOfOriginalRightPremise,
		       Axiom1b),
	  isAxiom(Axiom1b, _),
	  !
	 ;
	  writeln('Error on deduction theorem case of rewriting modus ponens.'),
	  abort
	 ),
	 
	 mpRewrite(LeftFormulaPrem, LeftRef, RightFormulaPrem, RightRef,
		   DischargingAssumption, MpConclusion, Key, FixedProof, ProofLines
		  )
	;
	 
	 /* only the Mp left premise is changed: we prepare the right premise */
	 leftPremiseIsChanged(LeftFormulaPrem, RightFormulaPrem,
			      DischargingAssumption, MpConclusion),
	 !,
	 
	 /* if in FixedProof we have DischargingAssumption->RightFormulaPrem we are ready
	    * to proceed otherwise we have to buid such a formula
	 */
	 Target = im(DischargingAssumption, RightFormulaPrem),
	 (
	  memberchk(line(KeyTarget, Target, _), FixedProof),
	  !,
	  mpRewrite(LeftFormulaPrem, LeftRef, Target, KeyTarget,
		    DischargingAssumption, MpConclusion, Key, FixedProof, ProofLines
		   )
	 ;

	  standardRewrite(LineOfRightPrem, DischargingAssumption, FixedProof, FixedProof2),
	  getLabel(FixedProof2, Target, Label2),
	  mpRewrite(LeftFormulaPrem, LeftRef, Target, Label2,
		    DischargingAssumption, MpConclusion, Key, FixedProof2, ProofLines
		   )
	 )
	;
	 /* only the Mp right premise is changed: we prepare the left premise as usual */
	 rightPremiseIsChanged(LeftFormulaPrem, RightFormulaPrem,
			       DischargingAssumption, MpConclusion),
	 !,
	 Target = im(DischargingAssumption, LeftFormulaPrem),
	 (
	  memberchk(line(KeyTarget, Target, _), FixedProof),
	  !,
	  mpRewrite(Target, KeyTarget, RightFormulaPrem, RightRef,
		    DischargingAssumption, MpConclusion, Key, FixedProof, ProofLines
		   )
	 ;
	  
	  standardRewrite(LineOfLeftPrem, DischargingAssumption, FixedProof, FixedProof2),
	  getLabel(FixedProof2, Target, Label2),
	  mpRewrite(Target, Label2, RightFormulaPrem, RightRef,
		    DischargingAssumption, MpConclusion, Key, FixedProof2, ProofLines
		   )
	 )
	
	).

/*
  We do not need to rewrite a mp application if the premises have not been rewritten
*/

nothingToDo(LeftFormula, RightFormula, Conclusion):-
	RightFormula = im(LeftFormula, Conclusion).


leftPremiseIsChanged(LeftFormula, RightFormula, DischargingAss, Conclusion):-
	RightFormula = im(X, Conclusion),
	LeftFormula = im(DischargingAss, X).


rightPremiseIsChanged(LeftFormula, RightFormula, DischargingAss, Conclusion):-
	Conclusion = Y,
	LeftFormula = X,
	RightFormula = im(DischargingAss, im(X, Y)).

bothPremisesAreChanged(LeftFormula, RightFormula, DischargingAss, Conclusion):-
	LeftFormula = im(DischargingAss, X),
	RightFormula = im(DischargingAss, im(X, Conclusion)).
/*

  Given the line (label, formula, type), add the lines
  
  (new1, formula->(DiscAss->formula), axiom)
  (new2, DischAss->formula, mp(label, new1))
  
*/

standardRewrite(InLine, DischargingAssumption, FixedProof, ProofLines):-
	InLine = line(Key, Formula, _), 
	newKey(NewKey1),
	Line1 = line(NewKey1, im(Formula, im(DischargingAssumption, Formula)), axiom),
	newKey(NewKey2),
	Line2 = line(NewKey2, im(DischargingAssumption, Formula), mp(Key, NewKey1)),
	append(FixedProof, [Line1, Line2], ProofLines).





mpRewrite(LeftFormulaPrem, LineLeftPrem,
	  RightFormulaPrem, LineRightPrem,
	  DischargingAssumption, Formula, Key,
	  FixedProof, ProofLines
	 ):-
	 newKey(NewKey1),
	 Line1 = line(NewKey1,
		      im(LeftFormulaPrem, im(RightFormulaPrem, im(DischargingAssumption, Formula))),
		      axiom
		     ),
	 
	 newKey(NewKey2),
	 Line2 = line(NewKey2,
		      im(RightFormulaPrem, im(DischargingAssumption, Formula)),
		      mp(LineLeftPrem, NewKey1)
		     ),
	 Line3 = line(Key, im(DischargingAssumption, Formula), mp(LineRightPrem, NewKey2)),
	 append(FixedProof, [Line1, Line2, Line3], ProofLines).
