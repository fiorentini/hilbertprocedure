\section{The Hilbert calculus  $\Hc$}
\label{sec:calculus}

We consider the propositional language $\Lcal$ of Classical
Propositional Logic ($\CPL$) based on a denumerable set of propositional
variables $\PV$ and the connectives~$\to$ and~$\neg$. The logical
connectives~$\land$ and~$\lor$ can be introduced by setting $A\land B=
\neg (A\to \neg B)$ and $A\lor B=\neg A\to B$.  A \emph{literal} is a
formula of the form $p$ or $\neg p$ with $p\in\PV$; the set of
literals is denoted by $\Lit$.

We call $\Hc$ the Hilbert calculus for $\CPL$ introduced by Kleene
in~\cite{Kleene:52}.  Logical axioms of $\Hc$ are:
\[
\begin{array}{ll}
  \Ax{1}\qquad &  A\to (B\to A)
  \\
  \Ax{2} & (A\to B)\to ((A\to (B\to C))\to (A\to C))
  \\
  \Ax{3} & (A\to B)\to ((A\to\neg B)\to \neg A)
  \\
  \Ax{4} &\neg\neg A\to A
\end{array}
\]
The only  rule of $\Hc$ is $\MP$ (\emph{Modus Ponens}):
from  $A\to B$ and $A$ infer $B$.
Let $\G$ be a set of formulas and $A$ a formula.
A \emph{deduction} of $A$ from assumptions $\Gamma$ is a finite sequence
of formulas $\deduction{A_1,\dots,A_n}$ such that $A_n=A$  and,
for every $A_i$ in the sequence, one of the following conditions holds:

\begin{enumerate}[label=(\alph*),ref=(\alph*)]

\item $A_i\in\Gamma$ (namely, $A_i$ is an assumption);

\item $A_i$ is an instance of a logical axiom;

\item $A_i$ is obtained by applying $\MP$ to $A_j$ and $A_k$,
  with $j < i$ and $k < i$.
\end{enumerate}

\noindent
By $\Dcal:\seqhc{\G}{A}$ we mean that $\Dcal$ is a deduction of $A$
from assumptions $\G$.  By $\Fm{\Dcal}$ we denote the set of formulas
occurring in the sequence $\Dcal$.
% In representing a deduction, we introduce annotations to specify the
% status of each formula.
In the following proposition we introduce some deductions:

\begin{proposition}\label{prop:ded}
  For every formula $A$, $B$ and $K$, the following deductions can be
  constructed:
  \begin{enumerate}
  \item\label{prop:ded:a} $\dedMP{A\to B,A}:\seqhc{A\to B,A}{B}$;
  \item\label{prop:ded:b} $\dedDoubleNeg{A}:\seqhc{\neg\neg A}{A}$;
  \item\label{prop:ded:c} $\dedEXF{A,B}:\seqhc{A,\neg A}{B}$;
  \item\label{prop:ded:d} $\dedIMP{A,K}:\seqhc{\neg A\to K,\neg
      A\to\neg K}{A}$;
  \item\label{prop:ded:e} $\dedIMPneg{A,K}:\seqhc{ A\to K, A\to\neg K}{\neg A}$.
    \qed
  \end{enumerate}
\end{proposition}



\noindent 
Let $\Dcal$ and $\Ecal$ be two
deductions, where $\Ecal=\deduction{A_1,\dots,A_n}$.
The \emph{concatenation} of $\Dcal$ and $\Ecal$, denoted
by $\Dcal\circ\Ecal$, is defined as follows:

\begin{itemize}
\item  let $\Ecal'$ be obtained by removing from  $\Ecal$ 
the formulas $A_j$ such that $1\leq j < n$ and
$A_j$ is in $\Dcal$;
then,  $\Dcal\circ\Ecal$ is  the concatenation
of the sequences  $\Dcal$ and $\Ecal'$.



% \item if $B\in \Fm{\Dcal}$, then $\Dcal\circ\Ecal$ is the deduction
% $\Dcal$ truncated at the first occurrence of $B$;

% \item Let $B\not\in \Fm{\Dcal}$ and let $\Ecal'$ be obtained by
%   removing from $\Ecal$ the formulas in $\Dcal$.  Then,
%   $\Dcal\circ\Ecal$ is  obtained by concatenating
%   the sequences $\Dcal$
%   and $\Ecal'$.
\end{itemize}

\noindent
One can easily check that:

\begin{lemma}\label{lemma:concatenation}
Let  $\Dcal:\seqhc{\G}{A}$ and $\Ecal:\seqhc{\Delta}{B}$. 
Then,  $\Dcal\circ\Ecal : \seqhc{\G\cup (\Delta\setminus\Fm{\Dcal}) }{B}$
and $\Fm{\Dcal\circ \Ecal}=\Fm{\Dcal}\cup\Fm{\Ecal}$.
  \qed
\end{lemma}

\noindent
A distinguishing feature of Hilbert calculi is that there are no rules
to close assumptions.  Thus, to prove the Deduction Lemma we have to
rearrange a deduction $\Dcal$ of $\seqhc{A,\Gamma}{B}$, as shown in
next lemma.

\begin{lemma} [Deduction Lemma]\label{lemma:deduction}
  Let $\Dcal:\seqhc{A,\Gamma}{B}$.  Then, there exists a deduction
  $\dedDedL{\Dcal}:\seqhc{\Gamma}{A\to B}$ such that, for every
  $C\in\Fm{\Dcal}$, $A\to C\in\Fm{\dedDedL{\Dcal}}$. 
  %%Moreover, if
  %%$C\in\Fm{\Dcal}\cap \G$, then $C\in\Fm{\dedDedL{\Dcal}}$.
  \qed
\end{lemma}

In the next lemma we introduce the deductions $\dedNegE{\Dcal}$
and $\dedINeg{\Dcal}$.

\begin{lemma}\label{lemma:neg}
\mbox{}
\begin{enumerate}[label=(\roman*),ref=(\roman*)]
\item\label{lemma:neg:1} Let $\Dcal:\seqhc{\neg A,\Gamma}{K}$ such
  that $\neg K\in\Fm{\Dcal}$.  Then, there exists a deduction
  $\dedNegE{\Dcal}:\seqhc{\Gamma}{A}$.


\item\label{lemma:neg:2} Let $\Dcal:\seqhc{A,\Gamma}{K}$ such that
  $\neg K\in\Fm{\Dcal}$.  Then, there exists a deduction
  $\dedINeg{\Dcal}:\seqhc{\Gamma}{\neg A}$.

\end{enumerate}
\end{lemma}

\begin{proof}
  We prove Point~\ref{lemma:neg:1}.  By the Deduction Lemma, there
  exists a deduction $\dedDedL{\Dcal}:\seqhc{\Gamma}{\neg A\to K}$
  such that $\neg A\to \neg K\in\Fm{\Dcal}$.  Let us consider the
  derivation $\dedIMP{A,K}:\seqhc{\neg A\to K,\neg A\to \neg K}{A}$
  defined in Proposition~\ref{prop:ded}.\ref{prop:ded:d}.  We can set
  $\dedNegE{\Dcal}=\dedDedL{\Dcal} \circ \dedIMP{A,K}$ which, by
  Lemma~\ref{lemma:concatenation}, is a deduction of
  $\seqhc{\Gamma}{A}$.  The definition of the deduction
  $\dedINeg{\Dcal}$ of Point~\ref{lemma:neg:2} is similar using
  $\dedIMPneg{A,K}$ instead of $\dedIMP{A,K}$.  \qed
\end{proof}


A \emph{(classical) interpretation} $\Ical$ is a subset of $\PV$; the
validity of a formula $A$ in $\Ical$, denoted by $\Ical\models A$, is
defined as usual.  Given a set of formulas $\Gamma$, $\Ical$ is a
\emph{model} of $\G$, denoted by $\Ical\models \Gamma$, iff
$\Ical\models C$, for every $C\in\Gamma$. A set of literals is
\emph{consistent} if it does not contain a complementary pair of
literals $\{p,\neg p\}$.  Given a consistent set of literals $\G$,
the interpretation $\G\cap\PV$ is a model of $\G$.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "hilbertProcedure"
%%% End: 
