\section{The Hilbert calculus  $\Hc$}
\label{sec:calculus}

We consider the propositional language $\Lcal$ of Classical
Propositional Logic ($\CPL$) based on a denumerable set of
propositional variables $\PV$ and the connectives~$\land$, $\lor$,
$\to$ and~$\neg$.  We write $A\Iff B$ as a shorthand for $(A\to B)
\land (B\to A)$.  A \emph{literal} is a formula of the form $p$ or
$\neg p$ with $p\in\PV$; the set of literals is denoted by $\Lit$.
The size of a formula $A$, denoted by $\size{A}$, is the number of
logical symbols occurring in $A$.
% The symbol $\bot$ is not part of the formula language, but it is an
% internal symbol used by the procedure \HpShort, described in
% Section~\ref{sec:procedure}, to represent an unsatisfiable formula.
% We define {\em atom} any formula in $\PV\cup\{\bot\}$.
A {\em model} $\Mcal$ is a subset of $\PV$; we write $\Mcal\sat A$ to
denote that the formula $A$ is \emph{valid in $\Mcal$}, according to
the usual definition.  Let $\G$ be a set of formulas; $\Mcal\sat\G$
iff, for every $A\in\G$, $\Mcal\sat A$. By $\G\sat A$ we mean that $A$
is a \emph{classical consequence} of $\G$, namely: for every model
$\Mcal$, $\Mcal\sat \G$ implies $\Mcal\sat A$.

% 
% A {\em binary formula} any formula whose outer connective is
% $\land,~\lor$ or~$to$.  Finally, let $A$ and $B$ formulas, then $A$
% is the {\em immediate subformula} of $\neg A$ and $A$ and $B$ are
% the {\em immediate subformulas} of $A\land B$, $A\lor B$ and $A\to
% B$.

We call $\Hc$ the Hilbert calculus for $\CPL$ introduced 
%% by Kleene
in~\cite{Kleene:52}.  Logical axioms of $\Hc$ are:
\[
\begin{array}[t]{l}
  \begin{array}{p{6ex}l}
    $\Ax{1}$ &  A\to (B\to A)\\
    $\Ax{2}$ & (A\to B)\to ((A\to (B\to C))\to (A\to C))
  \end{array}
  \\
  \begin{array}[t]{p{6ex}l}
    $\Ax{3}$ & A\to (B\to (A\land B))
  \end{array}
  \\
  \begin{array}[t]{p{6ex}lp{6ex}l}
    $\Ax{4a}$ & (A\land B)\to A\qquad\;\; & $\Ax{4b}$ & (A\land B)\to B\\
    $\Ax{5a}$ & A \to (A\lor B) & $\Ax{5b}$ & B \to (A\lor B) 
  \end{array}                                          
  \\
  \begin{array}[t]{p{6ex}l}
    $\Ax{6}$ & (A\to C)\to ((B\to C)\to ((A\lor B)\to C))
    \\
    $\Ax{7}$ & (A\to B)\to ((A\to\neg B)\to \neg A)
    \\
    $\Ax{8}$ &\neg\neg A\to A
  \end{array}
\end{array}
\]

% \[
% \begin{array}[t]{l}
%   \begin{array}{p{8ex}lp{8ex}l}
%     $\Ax{1}$ &  A\to (B\to A)
%     \qquad&
%     $\Ax{1a}$ & (A\to B)\to ((A\to (B\to C))\to (A\to C))
%   \end{array}
%   \\
%   \begin{array}[t]{p{8ex}l}
%     $\Ax{3}$ & A\to (B\to (A\land B))
%   \end{array}
%   \\
%   \begin{array}[t]{p{8ex}lp{8ex}l}
%     $\Ax{4a}$ & (A\land B)\to A\qquad\;\; & $\Ax{4b}$ & (A\land B)\to B\\
%     $\Ax{5a}$ & A \to (A\lor B) & $\Ax{5b}$ & B \to (A\lor B) 
%   \end{array}                                          
%   \\
%   \begin{array}[t]{ll}
%     \Ax{6}\quad & (A\to C)\to ((B\to C)\to ((A\lor B)\to C))
%     \\
%     \Ax{7} & (A\to B)\to ((A\to\neg B)\to \neg A)
%     \\
%     \Ax{8} &\neg\neg A\to A
%   \end{array}
% \end{array}
% \]

\noindent
The only  rule of $\Hc$ is $\MP$ (\emph{Modus Ponens}):
from  $A\to B$ and $A$ infer $B$.

Let $\G$ be a set of formulas and $A$ a formula.  A \emph{deduction}
of $A$ from assumptions $\Gamma$ is a finite sequence of formulas
$\Dcal=\deduction{A_1,\dots,A_n}$ (possibly with repetitions) such
that $A_n=A$ and, for every $A_i$ in the sequence, one of the
following conditions holds:

\begin{enumerate}[label=(\alph*),ref=(\alph*)]

\item $A_i\in\Gamma$ (namely, $A_i$ is an assumption);

\item $A_i$ is an instance of a logical axiom;

\item $A_i$ is obtained by applying $\MP$ to $A_j$ and $A_k$,
  with $j < i$ and $k < i$.
\end{enumerate}

\noindent
We denote with $\Fm{\Dcal}=\{A_1,\dots ,A_n\}$ the set of formulas
occurring in $\Dcal$.  By $\Dcal:\seqhc{\G}{A}$ we mean that $\Dcal$
is a deduction of $A$ from assumptions in $\G$.  Given two deductions
$\Dcal_1:\seqhc{\G_1}{A_1}$ and $\Dcal_2:\seqhc{\G_2}{A_2}$, we denote
by $\Dcal_1\circ\Dcal_2$ the deduction obtained by concatenating the
sequences $\Dcal_1$ and $\Dcal_2$.  One can easily check that
$\Dcal_1\circ\Dcal_2:\seqhc{\G}{A_2}$, where $\G\subseteq
\G_1\cup\G_2$.  Note that $\G$ can be a proper subset of
$\G_1\cup\G_2$, since some of the assumptions in $\G_2$ could be
obtained by applying $\MP$ in $\Dcal_1\circ\Dcal_2$ (see the next
example).

\begin{example}\em
  Let us consider the derivations
  \[
  \Dcal_1\;=\; \deduction{p_1,\,p_1\to p_2}
  \qquad
  \Dcal_2\;=\;\deduction{p_2,\,p_2\to (p_2\lor q),\, p_2\lor q} 
  \]
  
  \noindent  
  We have $\Dcal_1:\seqhc{p_1,p_1\to p_2\,}{\,p_1\to p_2}$.  In
  $\Dcal_2$, the formula $p_2\to (p_2\lor q)$ is an instance of axiom
  $\Ax{5a}$ and $p_2\lor q$ is obtained by applying $\MP$ to $p_2\to
  (p_2\lor q)$ and $p_2$, hence $\Dcal_2:\seqhc{p_2\,}{\,p_2\lor q}$.
  In the concatenation $\Dcal_1\circ\Dcal_2$ the formula $p_2$ can be
  obtained by applying $\MP$ to $p_1\to p_2$ and $p_1$, hence
  $\Dcal_1\circ\Dcal_2:\seqhc{p_1,p_1\to p_2}{p_2\lor q}$.\qed
\end{example}

In the following proposition we introduce some deduction schemas we
use later on.  
%%To represent a deduction, we list its formulas together with some
%%information on them.

\begin{lemma}\label{prop:ded}
  For all formulas $A$, $B$ and $C$, the following deductions can be
  constructed:
  \begin{enumerate}[label=(\roman*),ref=(\roman*)]
  \item\label{prop:ded:a} $\dedMP{A\to B,A}:\seqhc{A\to B,A}{B}$;
  \item\label{prop:ded:b} $\dedDoubleNeg{A}:\seqhc{\neg\neg A}{A}$;
  \item\label{prop:ded:c} $\dedEXF{A,B}:\seqhc{A,\neg A}{B}$;
  \item\label{prop:ded:d} $\dedIMP{A,B}:\seqhc{\neg A\to B,\neg
      A\to\neg B}{A}$;
  \item\label{prop:ded:e} $\dedIMPneg{A,B}:\seqhc{ A\to B, A\to\neg B}{\neg A}$.
  \item\label{prop:ded:f} $\dedEOr{A, B, C}:\seqhc{ A\to C, B\to C, A\lor B}{ C}$.
  \end{enumerate}
\end{lemma}
\begin{proof}
  The following sequence of formulas proves Point~\ref{prop:ded:f}:
  \[
  \begin{array}[t]{lll}
    (1)&  A\to C & \Ass\\
    (2)&  B\to C & \Ass\\
    (3)&  A\lor B& \Ass\\
    (4)&  (A\to C)\to & \\
    & ((B\to C)\to ((A\lor B)\to C)) & \Ax{6}\\
    (5)&  (B\to C)\to ((A\lor B)\to C) & \MP\, (4)\,(1)\\
    (6)&  (A\lor B)\to C & \MP\, (5)\, (2)\\
    (7)&  C &  \MP\, (6)\, (3)\quad\quad\qed\\
  \end{array}
  \]
\end{proof}

\noindent
A distinguishing feature of Hilbert calculi is that there are no rules
to close assumptions.  Thus, to prove the Deduction Lemma for $\Hc$ we have to
rearrange a deduction $\Dcal$ of $\seqhc{A,\Gamma}{B}$ to get a
deduction of $\seqhc{\Gamma}{A\to B}$. An analogous issue holds for
negation elimination and negation introduction. The following lemma
provides the schemas of derivation transformations treating such
cases:

\begin{lemma}[Closing assumption lemma]\label{lemma:closingAss}
  %%\label{lemma:deduction}
  \mbox{}
  \begin{enumerate}[label=(\roman*),ref=(\roman*)]
  \item\label{lemma:deduction} Let $\Dcal:\seqhc{A,\Gamma}{B}$.  Then, there exists a
    deduction $\dedIIm{\Dcal, A}:\seqhc{\Gamma}{A\to B}$ such that,
    for every $C\in\Fm{\Dcal}$, $A\to C\in\Fm{\dedDedL{\Dcal}}$.

  \item\label{lemma:neg:1} Let $\Dcal:\seqhc{\neg A,\Gamma}{K}$ such
    that $\neg K\in\Fm{\Dcal}$.  Then, there exists a deduction
    $\dedNegE{\Dcal, \neg A}:\seqhc{\Gamma}{A}$.
    
    
  \item\label{lemma:neg:2} Let $\Dcal:\seqhc{A,\Gamma}{K}$ such that
    $\neg K\in\Fm{\Dcal}$.  Then, there exists a deduction
    $\dedINeg{\Dcal, A}:\seqhc{\Gamma}{\neg A}$.
  \end{enumerate}
\end{lemma}

\begin{proof}
  For Point~\ref{lemma:deduction} see the proof of the Deduction Lemma
  in~\cite{Kleene:52}. As for Point~\ref{lemma:neg:1}, by
  Point~\ref{lemma:deduction} there exists a deduction
  $\dedDedL{\Dcal,\neg A}:\seqhc{\Gamma}{\neg A\to K}$ such that $\neg
  A\to \neg K\in\Fm{\dedDedL{\Dcal,\neg A}}$.  Let us consider the
  derivation $\dedIMP{A,K}:\seqhc{\neg A\to K,\neg A\to \neg K}{A}$
  defined in Lemma~\ref{prop:ded}.\ref{prop:ded:d}.  We set
  $\dedNegE{\Dcal,\neg A}:\seqhc{\Gamma}{A}$ as the deduction
  $\dedDedL{\Dcal,\neg A} \circ \dedIMP{A,K}$.  The deduction
  $\dedINeg{\Dcal,A}$ of Point~\ref{lemma:neg:2} is built in a similar
  way using $\dedIMPneg{A,K}$ instead of $\dedIMP{A,K}$. \qed
\end{proof}


% \noindent
% A distinguishing feature of Hilbert calculi is that there are no rules
% to close assumptions.  Thus, to prove the Deduction Lemma we have to
% rearrange a deduction $\Dcal$ of $\seqhc{A,\Gamma}{B}$, as shown in
% next lemma.

% \begin{lemma} [Deduction Lemma]\label{lemma:deduction}
%   Let $\Dcal:\seqhc{A,\Gamma}{B}$.  Then, there exists a deduction
%   $\dedIIm{\Dcal, A}:\seqhc{\Gamma}{A\to B}$ such that, for every
%   $C\in\Fm{\Dcal}$, $A\to C\in\Fm{\dedDedL{\Dcal}}$. 
%   %%Moreover, if
%   %%$C\in\Fm{\Dcal}\cap \G$, then $C\in\Fm{\dedDedL{\Dcal}}$.
%   %%\qed
% \end{lemma}

% In the next lemma we introduce the deductions $\dedNegE{\Dcal,\neg A}$
% and $\dedINeg{\Dcal, A}$.

% \begin{lemma}\label{lemma:neg}
% \mbox{}
% \begin{enumerate}[label=(\roman*),ref=(\roman*)]
% \item\label{lemma:neg:1} Let $\Dcal:\seqhc{\neg A,\Gamma}{K}$ such
%   that $\neg K\in\Fm{\Dcal}$.  Then, there exists a deduction
%   $\dedNegE{\Dcal, \neg A}:\seqhc{\Gamma}{A}$.


% \item\label{lemma:neg:2} Let $\Dcal:\seqhc{A,\Gamma}{K}$ such that
%   $\neg K\in\Fm{\Dcal}$.  Then, there exists a deduction
%   $\dedINeg{\Dcal, A}:\seqhc{\Gamma}{\neg A}$.

% \end{enumerate}
% \end{lemma}

% \begin{proof}
%   We prove Point~\ref{lemma:neg:1}.  By the Deduction Lemma, there
%   exists a deduction $\dedDedL{\Dcal}:\seqhc{\Gamma}{\neg A\to K}$
%   such that $\neg A\to \neg K\in\Fm{\Dcal}$.  Let us consider the
%   derivation $\dedIMP{A,K}:\seqhc{\neg A\to K,\neg A\to \neg K}{A}$
%   defined in Lemma~\ref{prop:ded}.\ref{prop:ded:d}.  We can set
%   $\dedNegE{\Dcal}=\dedDedL{\Dcal} \circ \dedIMP{A,K}$ which is a
%   deduction of $\seqhc{\Gamma}{A}$.  The definition of the deduction
%   $\dedINeg{\Dcal}$ of Point~\ref{lemma:neg:2} is similar using
%   $\dedIMPneg{A,K}$ instead of $\dedIMP{A,K}$. \qed
% \end{proof}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "hilbertProcedureEval"
%%% End: 

