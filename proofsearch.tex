\section{Procedure \HpShort}
\label{sec:procedure}

We present the procedure \HpShort to search for a deduction in $\Hc$.
Let $\G$ be a set of formulas and let $G$ be the goal formula or the
special symbol $\emptygoal$ representing the empty goal.  We define
the procedure \HpShort satisfying the following properties:

\begin{enumerate}[label=(H\arabic*),ref=(H\arabic*)]
  % #H1
\item\label{Hp:prop1} 
  If $G\in\Lcal$, $\HpShort(\G,G)$ returns either a deduction
  $\Dcal:\seqhc{\G}{G}$ or a model of $\G\cup\{\neg G\}$.
  
  % #H2
\item\label{Hp:prop2} $\HpShort(\G, \emptygoal)$ returns either a
  deduction $\Dcal:\seqhc{\G}{K}$ such that $\neg K\in \Fm{\Dcal}$ or
  a model of $\G$.
\end{enumerate}

\newenvironment{mytab}
{
  \\
  \begin{tabular}{p{0.1ex}p{.97\linewidth}}
  }
  {
  \end{tabular}
}

%%
\input{proc.tex}

\subsection*{Termination and completeness}

To search for a derivation of a goal  $G_0$ from assumptions $\G_0$, we have
to compute $\Hp(\G_0, G_0)$.  We show that the call $\Hp(\G_0, G_0)$
terminates.  The stack of recursive calls involved in the computation
of $\Hp(\G_0, G_0)$ can be represented by a chain
of the form
\[
\seqhc{\G_0}{G_0} \;\mapsto\; \seqhc{\G_1}{G_1}\;\mapsto\dots\mapsto\;
\seqhc{\G_n}{G_n}
\]  
where, for every $k\geq 0$, $\G_k$ is a set of formulas and the goal $G_k$ is a
formula or $\emptygoal$.
Each sequent $\s_k=\seqhc{\G_k}{G_k}$ in
the chain represents a call $\Hp(\G_k,G_k)$ performed along the
computation of $\Hp(\G_0, G_0)$.
%By $\s_k\mapsto\s_{k+1} $ we
%mean that in the computation of $\Hp(\G_k,G_k)$ there occurs a
%recursive call to $\Hp(\G_{k+1},G_{k+1})$.
%We call $\chain{\s_0}$ the chain starting from $\s_0$.
By $\Gcal_k$ we denote the set
$\G_k\cup\{\neg G_k\}$ if $G_k\neq \emptygoal$ and the set $\G_k$ if
$G_k=\emptygoal$.%  We say that $\Gcal_k$ is \emph{contradictory} iff
% there exists a formula $X$ such that both $\{X,\neg
% X\}\subseteq\Gcal_k$.

We have to prove that, for every set of formulas $\G_0$ and goal $G_0$,
every chain starting from $\s_0=\seqhc{\G_0}{G_0}$ is finite
(namely, the chain contains finitely many sequents).
Let us assume, by absurd,  that there  exists an infinite chain $\chain{\s_0}=\s_0\mapsto\s_1\mapsto
\s_2\dots $; we show that we get a contradiction.
To this aim, we state some properties of $\chain{\s_0}$
(the related proofs are  only sketched).
 
\begin{enumerate}[label=(P\arabic*),ref=(P\arabic*)]
%#P1
\item\label{term:P1}
For every $k\geq 0$,  $G_k$ is not an axiom.
  % #P2
\item\label{term:P2}
For every $k\geq 0$, the set $\Gcal_k$ is not contradictory.
\end{enumerate}  

\noindent
Let us assume, by contradiction, that there exists $k\geq 0$ such that
$G_k$ is an axiom.  Then the call $\Hp(\G_k,G_k)$ immediately ends at
step~\ref{HpShort-ax}, hence $\s_k$ would be the last sequent of
$\chain{\s_0}$, against the assumption that $\chain{\s_0}$ is infinite.
This proves~\ref{term:P1}.

To prove~\ref{term:P2}, let us assume  that there
exists $k\geq 0$ such that $\Gcal_k$ is contradictory.  Then, there
is a formula $X$ such that $\{X,\neg X\}\subseteq \Gcal_k$.  If $\{X,\neg
X\}\subseteq \G_k$ or $G_k=\emptygoal$, then the call $\Hp(\G_k,G_k)$
immediately ends at step~\ref{HpShort-coppia-complementare}, a
contradiction.  Thus, let us assume $G_k\neq \emptygoal$ and either
$G_k\in\G_k$ or $\neg\neg G_k\in\G_k$.  In the first case, the call
$\Hp(\G_k,G_k)$ immediately ends at step~\ref{HpShort-ax}.  If
$\neg\neg G_k\in\G_k$, by inspecting $\Hp$ one can check that there
exists $j > k$ such that $G_k\in\G_j$   and $G_j=G_k$  (see
step~\ref{HpShort-neg-neg-elim}), hence $\s_j$  would be the last sequent of
$\chain{\s_0}$, a contradiction.  This concludes the proof of~\ref{term:P2}.

By~\ref{term:P2} and the properties of $\eval$,
%(see Lemma~\ref{lemma:preserv}),
we can prove that the
evaluation of a formula is preserved along the chain, as stated in the
next property:


\begin{enumerate}[label=(P\arabic*),ref=(P\arabic*),start=3]
%#P3
\item\label{term:P3}
  Let $\eval(A,\Gcal_k) = \tau$, with $\tau\in\{\true,\false\}$.
  Then, for every $j\geq k$,  $\eval(A,\Gcal_j) = \tau$.
\end{enumerate}  

\noindent
To prove~\ref{term:P3}, we can exploit Lemma~\ref{lemma:preserv},
since, by Point~\ref{term:P2}, all the sets $\Gcal_k$ are non-contradictory.

By $\Sfn{\s_0}$ we denote the set of the formulas $H$ and $\neg H$
such that $H$ is a subformula of a formula in $\s_0$.
By inspecting the definition  of $\Hp$,
one can easily check that:

\begin{enumerate}[label=(P\arabic*),ref=(P\arabic*),start=4]
%#P6
\item\label{term:P4}
For every $k\geq 0$,  $\Gcal_k\subseteq \Sfn{\s_0}$.

\item\label{term:P5}
  For every $i\leq j$,  $\Gcal_i\cap\Lneg\,\subseteq\, \Gcal_j\cap \Lneg$.
\end{enumerate}  

\noindent
We now state some crucial properties of disjunctive goals:

\begin{enumerate}[label=(P\arabic*),ref=(P\arabic*),start=6]
%#P6
\item\label{term:P6}
  Let $G_k=A\lor B$ and $\eval(\G_k,A)\neq \false$.
  Then, there exists $j> k$ such that
  $\eval(\G_j,A)=\false$.
%#P7
\item\label{term:P7}
  Let $G_k=A\lor B$ and $\eval(\G_k,A)=\false$ and
   $\eval(\G_k,B)\neq\false$.
  Then, there exists $j> k$ such that
  $\eval(\G_j,A)=\eval(\G_j,B)=\false$.
%#P8
\item\label{term:P8}
  Let $H=\neg(A\lor B)$ such that
  $H\in\G_k$ and  $\eval(H,\G_k\setminus\{H\})\neq \true$.
  Then, there exists $j> k$ such that
  $H\in \G_j$ and
  $\eval(H,\G_j\setminus\{H\})= \true$.
\end{enumerate}

\noindent
Let us consider Point~\ref{term:P6}.
If $G_k=A\lor B$ and $\eval(\G_k,A)\neq \false$,
then there exists $l\geq k$ such that chain starting from the sequent $\s_l$ has the form
\[
  \begin{array}{l}
\seqhc{\G_l}{A\lor B}\;\mapsto \seqhc{\G_{l+1}}{A}\;\mapsto\;\cdots ;\mapsto
    \seqhc{\G_{l+m}}{H}
%    \\[.5ex]
%\mbox{$\G_{k}\subseteq \G_{k+1}\subseteq\cdots\subseteq \G_{m}$}    
  \end{array}
\]
where the formula $H$ 
satisfies one of the following properties:

\begin{itemize}
\item[(a)] $H=H_0\lor H_1$ and $\eval(H_0\lor H_1,\G_{l+m})\neq \false$;

  \item[(b)] $H$ is a literal.
\end{itemize}

\noindent
Indeed, after  a (possibly empty) initial phase where  some of the formulas in $\G_k$ are handled,  
the goal formula  $A\lor B$ is eagerly decomposed until a disjunctive formula $H$ satisfying~(a)
or a literal $H$ is obtained.
In this phase,
whenever we get a goal $G_j=B\to C$, the formula $B$ is added to the
left-hand side and the next sequent of the chain is 
$\seqhc{\G_{j},B}{C}$ (see step~\ref{HpShort-F-im-no-dx-local});
this might introduce a sequence of steps to decompose the formula $B$
and its subformulas.
By exploiting~\ref{term:P3}, 
one can  prove that:

\begin{itemize}
\item[(c)] $\eval(H ,\G_{l+m+1})=\false$
  implies $\eval(A ,\G_{l+m+1})=\false$.
\end{itemize}

\noindent
If  $H$ satisfies~(a),
the next  sequent
of the chain is  $\s_{l+m+1}=\seqhc{\G_{l+m},\neg H}{\emptygoal}$
(see step~\ref{HpShort-contr-F-or-no-dx-local}).
Since   $\eval(H ,\G_{l+m+1})=\false$,
by~(c) we get   $\eval(A,\G_{l+m+1})=\false$, hence Point~\ref{term:P6} holds.
Similarly,
if $H$ is a propositional variable,
then $H\not\in\G_{l+m}$, otherwise the
set $\Gcal_{l+m}$ would be contradictory, against
Point~\ref{term:P2}.
Thus, the next sequent
of the chain is  $\s_{l+m+1}=\seqhc{\G_{l+m},\neg H}{\emptygoal}$
(see step~\ref{HpShort-contr-F-or-no-dx-local}),
and this proves  Point~\ref{term:P6}. 
The case $H=\neg p$, with $p\in\PV$, is similar
(see step~\ref{HpShort-F-neg-no-dx-local}). 
The proofs of points~\ref{term:P7} and ~\ref{term:P8} are similar.


By the above points, it follows that we eventually get a sequent $\s_m$ such that $\G_m$ is reduced and
$G_m=\emptygoal$.
Indeed, by~\ref{term:P4}, the formulas occurring in $\chain{\s_0 }$  are subformulas of
$\Sfn{\s_0}$. Along the chain, 
formulas $H$ occurring in $\G_k$ such that $H\not\in\Lneg$ are eventually decomposed,
formulas $H\in\Lneg$ are preserved (see~\ref{term:P5}).
Formulas of the kind $\neg p$, with $p\in\PV$, does not threaten termination.
Let us take a formula $H=\neg(A\lor B)$ occurring in some
$\G_k$. By~\ref{term:P8}, there exists $l > k$ such that
$\s_l=\seqhc{\G_l}{G_l}$ and
$\eval(H,\G_l\setminus\{H\})=\true$.
By~\ref{term:P3}, for every $j\geq l$ it holds that
$\eval(H,\G_j\setminus\{H\})=\true$.
Since the formulas of the kind $\neg(A\lor B)$
that can occur in the sets $\G_k$ are finitely many,
it follows that we eventually
get a sequent $\s_m$ such that $\G_m$ is reduced and
$G_m=\emptygoal$. Since the only step applicable to the
call $\Hp(\G_m,G_m)$ is~\ref{HpShort-last},  $\s_m$ should be the last
sequent of $\chain{\s_0}$, a contradiction.
This proves that $\chain{\s_0}$ is finite,
thus: 
%%hence the call $\Hp(\G_0,G_0)$ eventually terminates. Thus:

\begin{theorem}[Termination]\label{th:term}
  Let $\G$ be a finite set of formulas of $\Lcal$ and $G$ a formula of $\Lcal$
  or $\emptygoal$. 
  Then, $\HpShort(\G, G)$ terminates.
\end{theorem}

Now, we prove that $\Hp$ is correct, namely: 

\begin{theorem}[Correctness]\label{th:corr}
  Let $\G$ be a finite set of formulas of $\Lcal$ and $G$ a formula of $\Lcal$
  or $\emptygoal$. 
  Then,
  $\HpShort(\G, G)$ satisfies properties~\ref{Hp:prop1}
  and~\ref{Hp:prop2}.
\end{theorem}

\begin{proof}
  By induction on the length $l$ of $\chain{\seqhc{\G}{G}}$.  If
  $l=0$, then the computation of $\HpShort(\G, G)$ does not require
  any recursive call.  Accordingly, one of the steps~\ref{HpShort-ax},
  \ref{HpShort-coppia-complementare} or~\ref{HpShort-last} is
  executed. In the first two cases, a derivation
  satisfying properties~\ref{Hp:prop1} and~\ref{Hp:prop2} is returned.
    In the latter case, the model $\Mcal=\G\cap\PV$ is returned.
  Since none of the conditions in the if-statements of
  cases~\ref{HpShort-ax}--\ref{HpShort-T-im} holds,
the set $\G$ is reduced and
  $G=\emptygoal$; by Theorem~\ref{th:reduced}, we conclude
  $\Mcal\sat\G$.
  % and property~\ref{Hp:prop2} holds.
  Let $l >1$ and let
  \[
  \chain{\seqhc{\G}{G}}\;=\;
  \seqhc{\G}{G}\mapsto\seqhc{\G_1}{G_1}\;\mapsto\; \cdots
  \]
  Since the length of $\chain{\seqhc{\G_1}{G_1}}$ is $l-1$, by
  induction hypothesis the call $\HpShort(\G_1, G_1)$ satisfies
  properties~\ref{Hp:prop1} and~\ref{Hp:prop2}.  By a case analysis,
  one can easily check that $\HpShort(\G, G)$
  satisfies~\ref{Hp:prop1} and~\ref{Hp:prop2} as well.  For instance,
  let us assume that $G=A_0\lor A_1$
and that step~\ref{HpShort-F-or} is executed.
If $\eval(A_0,\G)=\false$, the  recursive call
$\HpShort(\G, A_0)$ is executed.
If a deduction $\Dcal$ is returned,
by induction hypothesis $\Dcal$ is a deduction of $\seqhc{\G}{A_0}$. 
This implies that 
the deduction $\calD\circ \langle A_0\to (A_0\lor A_1), A_0\lor A_1\rangle$
returned by  $\HpShort(\G, A_0\lor A_1)$   is a deduction of  $\seqhc{\G}{A_0\lor A_1}$
(note that $A_0\to (A_0\lor A_1)$ is an instance of the axiom
$\Ax{5a}$).
Let us assume that $\HpShort(\G, A_0)$ returns a model $\Mcal$.
By induction hypothesis, we have $\Mcal\sat \G\cup\{\neg A_0\}$.
Moreover, since the if-condition in step~\ref{HpShort-contr-F-or-no-dx-local}
is false, it holds that   $\eval(A_0\lor A_1,\G)=\false$.
By Theorem~\ref{th:sat}, we get $\Mcal\sat\neg (A_0\lor A_1)$,
hence $\Mcal\sat\G\cup\{\neg(A_0\lor A_1\}$.  %which proves~\ref{Hp:prop1}.
The proof of the remaining subcases is similar.
%%% OTHER CASE ...
  % $\eval(A_0,\G)=\eval(A_1,\G)=\false$.  Then the recursive call
  % $\HpShort(\G, \emptygoal)$ in step~\ref{HpShort-F-or} is executed.  Let us
  % assume that such a call returns a model $\Mcal$; by induction
  % hypothesis, $\Mcal\models \G$.  By Theorem~\ref{th:sat}, it follows
  % that $\Mcal\nsat A_0$ and $\Mcal\nsat A_1$, hence $\Mcal\sat\neg(
  % A_0\lor A_1)$, which proves~\ref{Hp:prop1}.
\qed
\end{proof}


\input{example}

% \newpage
% \subsection{OLD}
% In Step~\ref{HpShort-contr-F-or-no-dx-local} procedure \HpShort uses {\em reductio ad
%   absurdum}. Note that propositional variables and disjunctions are the only kind of
% formulas involved in this step. The check $\eval(G, \G)\neq \false$ avoids to fall in an
% infinite loop. Step~\ref{HpShort-F-or} handles the case of a disjunctive formula that if
% provable from $\G$ does not require Step~\ref{HpShort-contr-F-or-no-dx-local}.
% Step~\ref{HpShort-T-neg-or} takes into account negated disjunctive formulas. This kind of
% negation must be handled as a separate case, since in the recursive call it belongs to
% $\G$ and it cannot be discharged because of the check $\eval(G,\G)\neq\false$ of
% Step~\ref{HpShort-contr-F-or-no-dx-local}.
% %
% % Finally, Step~\ref{HpShort-neg-neg-elim} is an optimization step and we could discharge it
% % without affecting termination and completeness.

% \begin{theorem}[CompletenessOLD]
%   \label{th:completenessOLD}
%   If $\HpShort(\G, G)$ returns a model $\Mcal$, then $\Mcal\sat \G$ and $\Mcal\nsat G$. 
% \end{theorem} {\em Proof}: we remark that \HpShort returns a model when last step is
% reached. In this case every formula in $\G$ is a literal or of the kind $\neg(B\lor C)$,
% where $\eval(\neg(B\lor C), \G)=\true$ and $G=\emptygoal$. We get the assertion by
% Theorem~\ref{th:eval}, Step~\ref{HpShort-last} and structural induction on the formulas in
% $\G\cup G$.
% \begin{theorem}[terminationOLD]
% Let $\G\subset \calL$ be finite and let $G\in\calL$. Then $\HpShort(\G, G)$ terminates.
% \end{theorem}
% %%
% {\em Proof:} we notice that to decide $\seqhc{\G}{G}$, procedure \HpShort handles
% subformulas occurring in $\G\cup\{G\}$ and, because of
% Step~\ref{HpShort-contr-F-or-no-dx-local}, negations of propositional variables and
% negations of disjunctive subformulas occurring in $\G\cup\{G\}$. Thus the number of ways
% in which $\G$ and $G$ can be instantiated is finite. Moreover, procedure \HpShort behaves
% as follows: 
% \begin{itemize}
% \item negated propositional variables occurring in $\G$ are not handled;
% \item for every occurrence of subformula in $\G\cup\{G\}$,
%   Step~\ref{HpShort-contr-F-or-no-dx-local} applies at most once; 
% \item for every occurrence of subformula in $\G\cup\{G\}$, Steps~\ref{HpShort-F-or}
%   and~\ref{HpShort-T-neg-or} applies at most twice;
% \item  for every occurrence of subformula in $\G\cup\{G\}$, any step different from those
%   above applies once.
% \end{itemize}




%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "hilbertProcedureEval"
%%% End: 
